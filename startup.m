% Set default start directory
% $LastChangedDate: 2010-10-04 15:20:58 -0600 (Mon, 04 Oct 2010) $
% $Rev: 850 $
%
% Orignal by Chris Arndt
%
%example_sel2html('sysgen_example_sel.txt')
cd('C:\home\chrisar\')

%% -- Dynamic System Generator for DSP Setup -- %%
%xilinx_dsp=getenv('XILINX_DSP');

%if isempty(xilinx_dsp)
   %error('XILINX_DSP environment variable is not defined.');
%end

%sysgenbin=fullfile(xilinx_dsp, 'sysgen', 'bin');

%if exist(sysgenbin, 'dir')
   %addpath(sysgenbin);
   %xlAddSysgen(xilinx_dsp);
   %clear xilinx_dsp;
   %clear sysgenbin;
%else
   %error('Could not find bin directory for System Generator to be added into Matlab paths.  Please check your XILINX_DSP environment variable');
   %clear sysgenbin;
%end
%---------++++ 11.1 ++++---------%
my_env
