%%
% This Matlab script echos the current enviroment setting from within the current Matlab session.
% Original by Chris Arndt
%
% $Id: my_env.m 850 2010-10-04 21:20:58Z theeggbe $
% $LastChangedDate: 2010-10-04 15:20:58 -0600 (Mon, 04 Oct 2010) $
% $Rev: 850 $
%

!echo XILINX=%XILINX%
!echo XILINX_DSP=%XILINX_DSP%
!echo PATH=%PATH%
!vcom -version
!echo MODELSIM=%MODELSIM%
xlVersion
