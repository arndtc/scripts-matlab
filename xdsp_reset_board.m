%% Program the Nallatech board using the Nallatech-supplied dll files
% This roughly follows the programming example in
% $FUSE_INTSALL\XtremeDSPKit\Examples\host_interface_basic\CCodeTester
% The this example includes Chipscope so that you can monitor data
% transfers of the the PCI or USB interface.
% This script currenlty only works for 1 card, thought it should be too
% hard to modify for mulitple cards.
%
% Original by Andy Khan
% Modified by Chris Arndt
%
% v1.1 - 10.23.2004
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

%% Script Setup
% This will setup the source bitfiles
% Setup the Nallatech FPGA dime information
% Point to the FUSE install
% Find the Windows install directory
% Load the Nallatech variable definitions

FPGA_clock_file = 'osc_clock_2v80.bit';	% uses the onboard 65 MHz crystal to clock main FPGA
FPGA_main_file = 'host_interface.bit';

ModuleNumber=0;
PrimaryFPGADeviceNum=1; 
SecondaryFPGADeviceNum=0;

CARD = 'USB';
FUSE_INSTALL = 'C:\opt\FUSE';
[x, SysRoot] = system('echo %SystemRoot%');
nallatech_varDefines


%% Load the Nallatech API libraries
disp('Loading Nallatech API libraries ...');
if ~(libisloaded('vidime'))
  loadlibrary([SysRoot(1:end-1),'\vidime.dll'],[FUSE_INSTALL,'\include\vidime.h'])
end
if ~(libisloaded('dimesdl'))
  loadlibrary([SysRoot(1:end-1),'\dimesdl.dll'],[FUSE_INSTALL,'\include\dimesdl.h'])
end
disp('Nallatech API libraries loaded.');

% Uncommment the line below to see the available library function calls.
% This is only for information, and not necessary for program operation.
%libfunctionsview('dimesdl');libfunctionsview('vidime')


%% Find and open the cards on PCI or USB interface
% If card a PCI or USB card is not found a handle error will be displayed.
% This error message is just an informational message.
if (CARD == 'PCI')
  disp('Searching for PCI cards ...');
  hLocate = calllib('dimesdl','DIME_LocateCard',def.dl.dlPCI,def.mbt.THEBENONE,0,0,0);

  NumOfCards = calllib('dimesdl','DIME_LocateStatus',hLocate,0,def.dl.NUMCARDS);
  if ( NumOfCards == 0 )
    error('Could not find any PCI cards.');
  else
    fprintf('Found %d PCI cards.\n',NumOfCards);    
  end    
elseif (CARD == 'USB')
  disp('Searching for USB cards ...');
  hLocate = calllib('dimesdl','DIME_LocateCard',def.dl.dlUSB,def.mbt.THEBENONE,0,0,0);

  NumOfCards = calllib('dimesdl','DIME_LocateStatus',hLocate,0,def.dl.NUMCARDS);
  if ( NumOfCards == 0 )
    error('Could not find any USB cards.');
  else
    fprintf('Found %d USB cards.\n',NumOfCards);    
  end
else
  error('No cards found!');   
end

for I = 1:NumOfCards,
	motherBoardType = calllib('dimesdl','DIME_LocateStatus',hLocate,I,def.dl.MBTYPE);
	fprintf('Card number %d has motherboard type %d.\n',I,motherBoardType);
end


%% Opening Card
disp('Opening a handle to card #1 ...')
hCard1 = calllib('dimesdl','DIME_OpenCard',hLocate,1,1);
disp('Xtreme DSP card is open.');


%% Enable Resets
disp('Enabling FPGA Reset ...');
if ( calllib('dimesdl','DIME_CardResetControl',hCard1,def.dr.ONBOARDFPGA,def.dr.ENABLE,0) )
	error('Could not enable FPGA Reset.');
end
disp('Enabling System Reset ...');
if ( calllib('dimesdl','DIME_CardResetControl',hCard1,def.dr.SYSTEM,def.dr.ENABLE,0) )
	error('Could not enable reset system FPGA.');
end
disp('Toggling Interface Reset ...');
if ( calllib('dimesdl','DIME_CardResetControl',hCard1,def.dr.INTERFACE,def.dr.TOGGLE,0) )
	error('Could not toogle Interface Reset.');
end
disp('Resets Enabled.');


%% Disable Resets
disp('Disabling FPGA Reset ...');
if ( calllib('dimesdl','DIME_CardResetControl',hCard1,def.dr.ONBOARDFPGA,def.dr.DISABLE,0) )
	error('Could not disable FPGA Reset.');
end
pause(.5);
disp('Disabling System Reset ...');
if ( calllib('dimesdl','DIME_CardResetControl',hCard1,def.dr.SYSTEM,def.dr.DISABLE,0) )
	error('Could not disable reset system FPGA.');
end
pause(.5);
disp('Toggling Interface Reset ...');
if ( calllib('dimesdl','DIME_CardResetControl',hCard1,def.dr.INTERFACE,def.dr.TOGGLE,0) )
	error('Could not toogle Interface Reset.');
end
pause(1);	% Give it a couple second for the embedded proc to sort out the DIMETalk network i.e. reset control etc.
disp('Resets Disabled.');


%% Closing Card
disp('Closing the card ...');
calllib('dimesdl','DIME_CloseCard',hCard1);
calllib('dimesdl','DIME_CloseLocate',hLocate);
disp('Xtreme DSP card is Closed.')


%% Unload the Nallatech API libraries
disp('Unloading Nallatech API libraries ...');
if (libisloaded('vidime'))
    unloadlibrary('vidime')
end
if (libisloaded('dimesdl'))
	unloadlibrary('dimesdl')
end
disp('Nallatech API libraries unloaded.')
