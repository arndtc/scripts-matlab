%creates 10 cycles of 100 pts, cos and sin
%binary format:  signed 2's 16_14
%
% Original by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

x = 0:.01:10;
cosx = cos(2*pi*x);
sinx = sin(2*pi*x);
x_in = dec2bword(cosx,16,14);
y_in = dec2bword(sinx,16,14);
