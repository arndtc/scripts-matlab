% Note to multiply 532 * 978 you would just enter mult_demo([5 3 2], [9 7 8])
%
% Original by Brian Hoard 9/8/2003
% Modified by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

% Note to multiply 532 * 978 you would just enter mult_demo([5 3 2], [9 7 8])


function y = mult_demo(a,b)

if length(a) ~= length(b)
    error('ERROR: Vectors must be equal length');
end

N = length(a); % N is the length of the vectors

% Rearrange MSB to LSB
for i = 0:1:(N-1)
    a_temp(i+1)=a(N-i);
    b_temp(i+1)=b(N-i);
end

a = a_temp;
b = b_temp;

% Extend them so they are "periodic":

a = [a zeros(1, N)];
b = [b zeros(1, N)];

% Script that computes the product of two numbers

% Two parts
% Stage #1: Get Coefficients Cn = AnBn
% Stage #2: Get un-normalized answer and normalize

% Evaluate the discrete circular convolution via FFTs-
% Take a<=>A and b<=>B and Create the DCT using the FFT values

A = fft(a);
B = fft(b);
fft_circ_conv = ifft(A.*B); % The 2*N scaling cancels out
Cn = fft_circ_conv;
Cn = real(Cn);

% Now we need to normalize the final values of the DCT

d(1)=mod(Cn(1), 10); % Using Base 10
car(1) = floor(Cn(1)/10);

for k=2:1:(length(Cn))
    v(k)=Cn(k)+car(k-1);
    d(k)=mod(Cn(k)+car(k-1),10);
    car(k)=floor((Cn(k)+car(k-1))/10);
end


% Now multiply by the appropirate power of 10 so we can sum them up!

for k = 0:1:(length(Cn)-1)
    d_temp(k+1)=d(length(Cn)-k)*10^(length(Cn)-k-1);
end

d = d_temp;


% Now sum them for the final answer!

final_sum = 0; % Initialize
for i = 1:1:length(Cn)
    final_sum = final_sum+d(i);
end

format short e
disp(' ');
disp(['Product Using Circular Convolution = ', num2str(final_sum)]);
disp(' ');














