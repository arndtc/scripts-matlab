function out = twoscompfrac2dec(x)
%Decimal to Binary Conversion 
%   twoscomp2dec(x) turns any binary 2's complement number into Decimal
%   Binary number of the specified number number of bits (n), with the
%   binary point being (b) positions to the left of the LSB.
%
%   The Syntax is:
%              C = twoscompfrac2dec('01010')
%    result -> C =  0.3125
%
% This version does not handle 'binary points' may add to a future version
%
% Original by Chris Arndt
%
% $LastChangedDate: 2009-01-19 14:04:44 -0700 (Mon, 19 Jan 2009) $
% $Rev: 633 $
%

%% initialise variables
temp_out = 0;
i = 0;

for i = 1:length(x)
  char = bin2dec(x(i));
%  if (i == 1 && char == 1)  % determine if number is negative
%      temp_out = temp_out - (char * 2^j);  % if number is negative msb sets negative value
%  else
      temp_out = temp_out + (char * 2^-i);
%  end
end

out = temp_out;
