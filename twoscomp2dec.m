function out = twoscomp2dec(x)
%Decimal to Binary Conversion 
%   twoscomp2dec(x) turns any binary 2's complement number into Decimal
%   Binary number of the specified number number of bits (n), with the
%   binary point being (b) positions to the left of the LSB.
%
%   The Syntax is:
%              C = twoscomp2dec('01010')
%    result -> C = 10
%
% This version does not handle 'binary points' may add to a future version
%
% Original by Chris Arndt
%
% $LastChangedDate: 2008-09-11 12:40:43 -0600 (Thu, 11 Sep 2008) $
% $Rev: 572 $
%

%% initialise variables
temp_out = 0;
i = 0;
j = length(x);

for i = 1:length(x)
  j = j - 1; %subtract 1 first because binary indexing is length(x) - 1 downto 0
  char = bin2dec(x(i));
  if (i == 1 && char == 1)  % determine if number is negative
      temp_out = temp_out - (char * 2^j);  % if number is negative msb sets negative value
  else
      temp_out = temp_out + (char * 2^j);
  end
end

out = temp_out;
