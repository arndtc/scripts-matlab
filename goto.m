function goto(filename)
% GOTO             Jump to a specified file
%
% GOTO filename changes MATLAB current directory to the one containing
% filename.  filename must be on the MATLAB path.  goto uses the first
% instance of filename that it finds (a la WHICH).
%
% SEE ALSO:  WHICH
%
%
% Original by Scott Hirsch shirsch@mathworks.com
% Modified by Chris Arndt
%
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

file = which(filename);
pathstr = fileparts(file);
cd(pathstr);
