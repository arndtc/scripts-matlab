%Xilinx Diagnostics Script
%
% This script uses the Xilinx Tools and Matlab enviroment to return debug information
% about the System Generator for DSP setup.
%
% Original by Chris Arndt
%
% $LastChangedDate: 2007-11-20 13:45:07 -0700 (Tue, 20 Nov 2007) $
% $Rev: 383 $
%


%% Set output filename
filename = 'xilinx_diag_output.txt';
mycwd = pwd;
filenameout = [mycwd, filesep, filename]

%% Find out if Xilinx enviroment variable is set.
disp('Gathering Setup Informaiton ...');
xilinxpath = getenv('XILINX');
if (~strcmp(xilinxpath, ''))
    xinfopath = [xilinxpath filesep 'bin' filesep 'nt' filesep 'xinfo.exe'];
    [status,res] = system([xinfopath, ' ', filenameout]);
end

%% Get system information specific to System Generator for DSP debugging
systempath = getenv('PATH');
[status,vcomresult] = system('vcom -version');
modelsimini = getenv('MODELSIM');
sysgenver = xlVersion;
matlabversion = version;
matlabver = ver;

%% Write output to file
fid = fopen(filenameout,'a');
fprintf(fid, '\n\n---- MATLAB ENVIRONMENT DISCOVERY RESULTS -----\n');
fprintf(fid, 'XILINX = %s\n\n', xilinxpath);
fprintf(fid, 'xlversion =\n');
for i = 1:length(sysgenver)
    fprintf(fid, '%s\n', sysgenver{i});
end
%fprintf(fid, 'xlversion = %s\n\n', sysgenver);
fprintf(fid, '\nVCOM Results = %s\n', vcomresult);
fprintf(fid, 'MODLESIM = %s\n\n', modelsimini);
fprintf(fid, 'systempath = %s\n\n', systempath);
fprintf(fid, 'version = %s\n\n', matlabversion);
fprintf(fid, 'ver =\n');
fprintf(fid, '%s\t\t\t\t%s\t\t\t%s\t\t%s\n', 'Release', 'Date', 'Version', 'Name');
fprintf(fid, '%s\n', '---------------------------------------------------------------------------');
for i = 1:length(matlabver)
    if (strcmp(matlabver(i).Release, 'production build'))
        fprintf(fid, '%s\t%s\t\t%s\t\t%s\n', matlabver(i).Release, matlabver(i).Date, matlabver(i).Version, matlabver(i).Name);
    else
        fprintf(fid, '%s\t\t\t%s\t\t%s\t\t\t%s\n', matlabver(i).Release, matlabver(i).Date, matlabver(i).Version, matlabver(i).Name);
    end
end

fclose(fid);

%%
disp('Complete.');
disp(['Please send the ', filenameout, ' file to the Xilinx Technical Support.']);

clear filename mycwd filenameout xilinxpath xinfopath status res systempath status vcomresult modelsimini sysgenver matlabversion matlabver fid i ans
