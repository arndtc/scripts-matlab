function xlTurnOffRPMs(modelName)
%% Find all Xilinx blocks in a subsystem or design that use RPMs 
%  and turn off the option
% 
%  Usage:  xlTurnOffRPMs(gcs)
%  Usage:  xlTurnOffRPMs(<model, subsystem name, or gcs>)
%
% For example, to find all the RPMs in the mod_sys design use the following command:
%  xlTurnOffRPMs('mod_sys')
%   
% Original by Nabeel Sharazi
% Modified by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%
   
%% Find all the blocks using RPMs 
blks = find_system(modelName,'LookUnderMasks', 'all', 'use_rpm', 'on');            
% This library part may or may not work
% To push into libraries, add the following option:  'FollowLinks', 'on',

numRPMs = length(blks);

if (numRPMs > 0)
    disp('Turning off RPMs on the following blocks:');
    for i = 1:numRPMs
        if (iscell(blks))
            blk = blks{i};
        else
            blk = blks(i);
        end;
        set_param(blk, 'use_rpm', 'off');
        str = sprintf('%s', blk);
        disp(str);
    end;
end;
str = sprintf('Total number of RPMs turned off: %d\n', numRPMs);
disp(str);

    
return;
                   
