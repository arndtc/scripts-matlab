function varargout = gen_jtag_cosim(varargin)
% GEN_JTAG_COSIM M-file for gen_jtag_cosim.fig
%      GEN_JTAG_COSIM, by itself, creates a new GEN_JTAG_COSIM or raises the existing
%      singleton*.
%
%      H = GEN_JTAG_COSIM returns the handle to a new GEN_JTAG_COSIM or the handle to
%      the existing singleton*.
%
%      GEN_JTAG_COSIM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GEN_JTAG_COSIM.M with the given input arguments.
%
%      GEN_JTAG_COSIM('Property','Value',...) creates a new GEN_JTAG_COSIM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gen_jtag_cosim_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gen_jtag_cosim_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gen_jtag_cosim

% Last Modified by GUIDE v2.5 06-Oct-2003 18:16:38
%
%
% Original by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gen_jtag_cosim_OpeningFcn, ...
                   'gui_OutputFcn',  @gen_jtag_cosim_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gen_jtag_cosim is made visible.
function gen_jtag_cosim_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gen_jtag_cosim (see VARARGIN)

% Choose default command line output for gen_jtag_cosim
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gen_jtag_cosim wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gen_jtag_cosim_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  this is where code editing starts
%
%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% --- Executes on button press in pbtn_generate.
function pbtn_generate_Callback(hObject, eventdata, handles)
% hObject    handle to pbtn_generate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%
h_obj = findobj(gcf, 'Tag', 'edit_clkpin');
str_clkpin = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_clkspeed');
str_clkspeed = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_vendor');
str_vendor = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_boardname');
str_boardname = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_family');
str_family = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_part');
str_part = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_speed');
str_speed = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_package');
str_package = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_position');
str_position = get(h_obj,'String');
h_obj = findobj(gcf, 'Tag', 'edit_chain');
str_chain = get(h_obj,'String');

str_filename = [str_vendor, '_', str_boardname];
%
int_postgeneration(str_filename, str_position, str_chain);
int_target(str_filename, str_family, str_part, str_speed, str_package, str_clkspeed, str_clkpin);
int_xltarget(str_filename, str_vendor, str_boardname);
int_ucf(str_filename, str_clkpin, str_clkspeed);


   
% --- Executes during object creation, after setting all properties.
function edit_boardname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_boardname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_boardname = edit_boardname_Callback(hObject, eventdata, handles)
% hObject    handle to edit_boardname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_boardname as text
%        str2double(get(hObject,'String')) returns contents of edit_boardname as a double
str_boardname = get(hObject,'string');



% --- Executes during object creation, after setting all properties.
function edit_clkpin_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end
%str_clkpin = get(hObject,'String')

function str_clkpin = edit_clkpin_Callback(hObject, eventdata, handles)
str_clkpin = get(hObject,'String');


% --- Executes during object creation, after setting all properties.
function edit_vendor_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_vendor = edit_vendor_Callback(hObject, eventdata, handles)
str_vendor = get(hObject,'string');


% --- Executes during object creation, after setting all properties.
function edit_clkspeed_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_clkspeed = edit_clkspeed_Callback(hObject, eventdata, handles)
%str_clkspeed = get(hObject,'string');



% --- Executes during object creation, after setting all properties.
function edit_chain_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_chain = edit_chain_Callback(hObject, eventdata, handles)
str_chain = get(hObject,'string');



% --- Executes during object creation, after setting all properties.
function edit_position_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_position = edit_position_Callback(hObject, eventdata, handles)
str_position = get(hObject,'string');



% --- Executes during object creation, after setting all properties.
function edit_family_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_family = edit_family_Callback(hObject, eventdata, handles)
str_family = get(hObject,'string');



% --- Executes during object creation, after setting all properties.
function edit_part_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_part = edit_part_Callback(hObject, eventdata, handles)
str_part = get(hObject,'string');



% --- Executes during object creation, after setting all properties.
function edit_speed_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_speed = edit_speed_Callback(hObject, eventdata, handles)
str_speed = get(hObject,'string');



% --- Executes during object creation, after setting all properties.
function edit_package_CreateFcn(hObject, eventdata, handles)
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function str_package = edit_package_Callback(hObject, eventdata, handles)
str_package = get(hObject,'string');




%
% file writing functions for creating the jtag co-sim files
%
%
%
function int_postgeneration(filename, position, chain)
fid=fopen([filename, '_postgeneration.m'], 'wt');
fprintf(fid, '%s\n', '%');
fprintf(fid, '%s\n', '% Filename:    yourboard_postgeneration.m');
fprintf(fid, '%s\n', '%');
fprintf(fid, '%s\n', '% Description: Template file that defines board-specific parameters ');
fprintf(fid, '%s\n', '%              (e.g. ucf file, non-memory mapped ports) before ');
fprintf(fid, '%s\n', '%              invoking the generic JTAG post-generation function.');
fprintf(fid, '%s\n', '%');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % (1) Rename the yourboard_postgeneration function.');
fprintf(fid, '%s%s%s\n', 'function st = ', filename, '_postgeneration(params)');
fprintf(fid, '%s\n', '  %           ~~~~~~~~~~~~~~~~~~~~~~~~        ');
fprintf(fid, '%s\n', '  ');
fprintf(fid, '%s\n', '  % (2) Set the value of the ''boundary_scan_position'' field to the ');
fprintf(fid, '%s\n', '  %     position of your FPGA in the Boundary-Scan chain (beginning at 1).');
fprintf(fid, '%s%s%s\n', '  params.(''boundary_scan_position'') = ''', position, ''';  ');
fprintf(fid, '%s\n', '  %                                    ~');
fprintf(fid, '%s\n', '                          ');
fprintf(fid, '%s\n', '  % (3) Set the value of the ''instruction_register_lengths'' field to the ');
fprintf(fid, '%s\n', '  %     vector of instruction register lengths in the Boundary-Scan chain.');
fprintf(fid, '%s%s%s\n', '  params.(''instruction_register_lengths'') = ''[', chain, ']''; ');
fprintf(fid, '%s\n', '  %                                          ~~~~~~');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % (4) Set the value of the ''ucf_template'' field to match the name of');
fprintf(fid, '%s\n', '  %     your board''s UCF file.');
fprintf(fid, '%s%s%s\n', '  params.(''ucf_template'') = ''', filename, '.ucf''; ');
fprintf(fid, '%s\n', '  %                          ~~~~~~~~~~~~~');
fprintf(fid, '%s\n', '  ');
fprintf(fid, '%s\n', '  % Specify board-specific I/O ports as follows (ensure the ports are');
fprintf(fid, '%s\n', '  % also declared in your board''s UCF file):');
fprintf(fid, '%s\n', '  % non_mm_ports.(''adc1_d'') = {''in'', 14};');
fprintf(fid, '%s\n', '  % non_mm_ports.(''dac1_d'') = {''out'', 14};');
fprintf(fid, '%s\n', '  % ...');
fprintf(fid, '%s\n', '  % params.(''non_memory_mapped_ports'') = non_mm_ports;');
fprintf(fid, '%s\n', '  ');
fprintf(fid, '%s\n', '  % You may use your own top-level netlist file by uncommenting the ');
fprintf(fid, '%s\n', '  % following line and setting the ''vendor_toplevel'' field accordingly.');
fprintf(fid, '%s\n', '  % params.(''vendor_toplevel'') = ''yourboard_toplevel'';');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % If you use your own top-level, you must tell SysGen what netlist ');
fprintf(fid, '%s\n', '  % files are required.  Set the ''vendor_netlists'' field to a cell ');
fprintf(fid, '%s\n', '  % array listing the required file names. ');
fprintf(fid, '%s\n', '  % params.(''vendor_netlists'') = {''yourboard_toplevel.ngc'',''foo.edf''};');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % Invoke the JTAG post-generation callback function to run');
fprintf(fid, '%s\n', '  % Xilinx tools and create a run-time co-simulation token.');
fprintf(fid, '%s\n', '  try');
fprintf(fid, '%s\n', '    st = xlJTAGPostGeneration(params); ');
fprintf(fid, '%s\n', '  catch');
fprintf(fid, '%s\n', '    errordlg([''-- An unkown error was encountered while running '' ...');
fprintf(fid, '%s\n', '             ''the JTAG hardware co-simulation flow.'']);');
fprintf(fid, '%s\n', '    st = 1;  ');
fprintf(fid, '%s\n', '  end   ');
fclose(fid);


function int_target(filename, family, part, speed, package, clkspeed, clkpin)
period = (1/(str2double(clkspeed)) * 1e3);  %conversion to ns
fid=fopen([filename, '_target.m'], 'wt');
fprintf(fid, '%s\n', '%');
fprintf(fid, '%s\n', '% Filename:    yourboard_target.m');
fprintf(fid, '%s\n', '%');
fprintf(fid, '%s\n', '% Description: Template file that defines the supported and default');
fprintf(fid, '%s\n', '%              compilation settings on the SysGen GUI for a given');
fprintf(fid, '%s\n', '%              board.');
fprintf(fid, '%s\n', '%');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % (1) Rename the yourboard_target function.');
fprintf(fid, '%s%s%s\n', 'function settings = ', filename, '_target');
fprintf(fid, '%s\n', '  %                 ~~~~~~~~~~~~~~~~');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % (2) Set the values of the ''family'', ''part'', ''speed'' and');
fprintf(fid, '%s\n', '  %     ''package'' fields to match the FPGA device on your board.');
fprintf(fid, '%s%s%s\n', '  my_part.(''family'') = ''', family, ''';');
fprintf(fid, '%s\n', '  %                     ~~~~~~~');
fprintf(fid, '%s%s%s\n', '  my_part.(''part'') = ''', part, ''';');
fprintf(fid, '%s\n', '  %                   ~~~~~~~~');
fprintf(fid, '%s%s%s\n', '  my_part.(''speed'') = ''', speed, ''';');
fprintf(fid, '%s\n', '  %                    ~~');
fprintf(fid, '%s%s%s\n', '  my_part.(''package'') = ''', package, ''';');
fprintf(fid, '%s\n', '  %                      ~~~~~');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  settings.(''supported_parts'').(''allowed'') = {my_part};');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % (3) Set the value of the ''sysclk_period'' field to match the');
fprintf(fid, '%s\n', '  %     period of the clock on your board (in ns).');
fprintf(fid, '%s%i%s\n', '  settings.(''sysclk_period'').(''allowed'') = ''', period, ''';');
fprintf(fid, '%s\n', '  %                                         ~~');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % (4) Rename the post-generation function.');
fprintf(fid, '%s%s%s\n', '  settings.(''postgeneration_fcn'') = ''', filename, '_postgeneration'';');
fprintf(fid, '%s\n', '  %                                  ~~~~~~~~~~~~~~~~~~~~~~~~');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % Testbench should not be generated for this target.');
fprintf(fid, '%s\n', '  settings.(''testbench'').(''allowed'') = ''off'';');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % Specify default target directory for this target.');
fprintf(fid, '%s\n', '  settings.(''directory'') = ''./netlist'';');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % List supported synthesis tools.');
fprintf(fid, '%s\n', '  settings.(''synthesis_tool'') = ''XST'';');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % Specify pre-compile callback function.');
fprintf(fid, '%s\n', '  settings.(''precompile_fcn'') = ''xlJTAGPreCompile'';');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % This target can be imported as a configurable subsystem.');
fprintf(fid, '%s\n', '  settings.(''getimportblock_fcn'') = ''xlGetHwcosimBlockName'';');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % Disable the clock location constraint field.');
fprintf(fid, '%s%s%s\n', '  settings.(''clock_loc'').(''allowed'') = ''', clkpin, ''';');
fprintf(fid, '%s\n', '');
fclose(fid);


function int_xltarget(filename, vendor, boardname)
fid=fopen('xltarget.m', 'wt');
fprintf(fid, '%s\n', '%');
fprintf(fid, '%s\n', '% Filename:    xltarget.m ');
fprintf(fid, '%s\n', '%');
fprintf(fid, '%s\n', '% Description: Template file that defines the target compilation   ');
fprintf(fid, '%s\n', '%              entry point for a given board.');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', 'function s = xltarget');
fprintf(fid, '%s\n', '  % Set the value of the ''name'' field to the name of your');
fprintf(fid, '%s\n', '  % compilation target (as you''d like it to appear in the ');
fprintf(fid, '%s\n', '  % SysGen GUI).');
fprintf(fid, '%s%s%s%s%s\n', '  s.(''name'') = ''', vendor, ' ', boardname, '(JTAG)'';');
fprintf(fid, '%s\n', '  %             ~~~~~~~~~~');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '  % Set the value of the ''target_info'' field to point to');
fprintf(fid, '%s\n', '  % yourboard_target.m.');
fprintf(fid, '%s%s%s\n', '  s.(''target_info'') = ''', filename, '_target'';');
fprintf(fid, '%s\n', '  %                    ~~~~~~~~~~~~~~~~');
fclose(fid);



function int_ucf(filename, clkpin, clkspeed)
fid=fopen([filename, '.ucf'], 'wt');
fprintf(fid, '%s\n', '#');
fprintf(fid, '%s\n', '# Filename:    yourboard.ucf');
fprintf(fid, '%s\n', '#');
fprintf(fid, '%s\n', '# Description: Template UCF file that defines the clock input');
fprintf(fid, '%s\n', '#              pin and frequency for the JTAG co-simulation');
fprintf(fid, '%s\n', '#              target.');
fprintf(fid, '%s\n', '#');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '# Set the sys_clk LOC constraint to match your board''s clock');
fprintf(fid, '%s\n', '# pin.');
fprintf(fid, '%s%s%s\n', 'NET "sys_clk" LOC = "', clkpin, '";');
fprintf(fid, '%s\n', '#                    ~~~~');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '# Set the sys_clk PERIOD constraint to match your board''s');
fprintf(fid, '%s\n', '# clock frequency.');
fprintf(fid, '%s%s%s\n', 'NET "sys_clk" PERIOD = ', clkspeed, ' MHz HIGH 50%;');
fprintf(fid, '%s\n', '#                      ~~~');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '# Constrain board-specific I/O ports below.  Remember to');
fprintf(fid, '%s\n', '# declare the same ports in the yourboard_postgeneration');
fprintf(fid, '%s\n', '# function.  Adding comments to each constraint ensures the');
fprintf(fid, '%s\n', '# constraint is removed from the UCF file when the port is');
fprintf(fid, '%s\n', '# not used in the SysGen design.');
fprintf(fid, '%s\n', '');
fprintf(fid, '%s\n', '# NET adc1_d(0) LOC = af20;                       # adc1_d contingent');
fprintf(fid, '%s\n', '# NET adc1_d(1) LOC = ad18;                       # adc1_d contingent');
fprintf(fid, '%s\n', '# NET adc1_d(2) LOC = ac22;                       # adc1_d contingent');
fprintf(fid, '%s\n', '');
fclose(fid);
