function varargout = waveform_creation(varargin)
% WAVEFORM_CREATION M-file for waveform_creation.fig
%      WAVEFORM_CREATION, by itself, creates a new WAVEFORM_CREATION or raises the existing
%      singleton*.
%
%      H = WAVEFORM_CREATION returns the handle to a new WAVEFORM_CREATION or the handle to
%      the existing singleton*.
%
%      WAVEFORM_CREATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WAVEFORM_CREATION.M with the given input arguments.
%
%      WAVEFORM_CREATION('Property','Value',...) creates a new WAVEFORM_CREATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before waveform_creation_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to waveform_creation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help waveform_creation

% Last Modified by GUIDE v2.5 16-Jun-2009 21:47:48
%
%
% Original by Chris Arndt
%
% $LastChangedDate: 2009-06-16 22:54:39 -0700 (Tue, 16 Jun 2009) $
% $Rev: 720 $
%

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @waveform_creation_OpeningFcn, ...
                   'gui_OutputFcn',  @waveform_creation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before waveform_creation is made visible.
function waveform_creation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to waveform_creation (see VARARGIN)

% Choose default command line output for waveform_creation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes waveform_creation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = waveform_creation_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbtn_write_wave_table.
function pbtn_write_wave_table_Callback(hObject, eventdata, handles)
% hObject    handle to pbtn_write_wave_table (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
int_write_file;


% --- Executes during object creation, after setting all properties.
function edit_out_freq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_out_freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function edit_out_freq_Callback(hObject, eventdata, handles)
% hObject    handle to edit_out_freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_out_freq as text
%        str2double(get(hObject,'String')) returns contents of edit_out_freq as a double
str_out_freq = get(hObject,'String');
if (length(str_out_freq) == 0)
  set(hObject,'String','1');
end
int_update_information;


% --- Executes during object creation, after setting all properties.
function edit_sample_freq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_sample_freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function edit_sample_freq_Callback(hObject, eventdata, handles)
% hObject    handle to edit_sample_freq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_sample_freq as text
%        str2double(get(hObject,'String')) returns contents of edit_sample_freq as a double
str_sample_freq = get(hObject,'String');
if (length(str_sample_freq) == 0)
  set(hObject,'String','1');
end
int_update_information;
int_update_information2;

% --- Executes during object creation, after setting all properties.
function edit_data_pts_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_data_pts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function edit_data_pts_Callback(hObject, eventdata, handles)
% hObject    handle to edit_data_pts (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_data_pts as text
%        str2double(get(hObject,'String')) returns contents of edit_data_pts as a double
str_data_pts = get(hObject,'String');
if (length(str_data_pts) == 0)
  set(hObject,'String','1');
end
int_update_information;
int_update_information2;


% --- Executes during object creation, after setting all properties.
function edit_bit_width_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_bit_width (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function edit_bit_width_Callback(hObject, eventdata, handles)
% hObject    handle to edit_bit_width (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_bit_width as text
%        str2double(get(hObject,'String')) returns contents of edit_bit_width as a double
str_bit_width = get(hObject,'String');
if (length(str_bit_width) == 0)
  set(hObject,'String','8');
end


% --- Executes on button press in rbtn_sin.
function rbtn_sin_Callback(hObject, eventdata, handles)
% hObject    handle to rbtn_sin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtn_sin
val_sin = get(hObject, 'Value');
if (val_sin == 1)
  h_obj = findobj(gcf, 'Tag', 'rbtn_cos');
  set(h_obj,'Value', 0);
end


% --- Executes on button press in rbtn_cos.
function rbtn_cos_Callback(hObject, eventdata, handles)
% hObject    handle to rbtn_cos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtn_cos
val_cos = get(hObject, 'Value');
if (val_cos == 1)
  h_obj = findobj(gcf, 'Tag', 'rbtn_sin');
  set(h_obj,'Value', 0);
end

% --- Executes on button press in rbtn_hex.
function rbtn_hex_Callback(hObject, eventdata, handles)
% hObject    handle to rbtn_hex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtn_hex
val_hex = get(hObject, 'Value');
if (val_hex == 1)
  h_obj = findobj(gcf, 'Tag', 'rbtn_dec');
  set(h_obj,'Value', 0);
end

% --- Executes on button press in rbtn_dec.
function rbtn_dec_Callback(hObject, eventdata, handles)
% hObject    handle to rbtn_dec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtn_dec
val_dec = get(hObject, 'Value');
if (val_dec == 1)
  h_obj = findobj(gcf, 'Tag', 'rbtn_hex');
  set(h_obj,'Value', 0);
end

function int_update_information()
h_obj = findobj(gcf, 'Tag', 'edit_out_freq');
str_out_freq = get(h_obj,'String');
fout = str2num(str_out_freq);

h_obj = findobj(gcf, 'Tag', 'edit_sample_freq');
str_sample_freq = get(h_obj,'String');
fs = str2num(str_sample_freq);

h_obj = findobj(gcf, 'Tag', 'edit_data_pts');
str_data_pts = get(h_obj,'String');
pts = str2num(str_data_pts);

%calculate Points per Cycle and update field
pts_per_cycle = fs / fout;
h_obj = findobj(gcf, 'Tag', 'txt_ptspc');
set(h_obj,'String',num2str(pts_per_cycle));

%calculate # of Cycles and update field
num_cycles = pts / pts_per_cycle;
h_obj = findobj(gcf, 'Tag', 'txt_cycles');
set(h_obj,'String',num2str(num_cycles));

function int_update_information2()
h_obj = findobj(gcf, 'Tag', 'edit_out_freq2');
str_out_freq = get(h_obj,'String');
fout = str2num(str_out_freq);

h_obj = findobj(gcf, 'Tag', 'edit_sample_freq');
str_sample_freq = get(h_obj,'String');
fs = str2num(str_sample_freq);

h_obj = findobj(gcf, 'Tag', 'edit_data_pts');
str_data_pts = get(h_obj,'String');
pts = str2num(str_data_pts);

%calculate Points per Cycle and update field
pts_per_cycle = fs / fout;
h_obj = findobj(gcf, 'Tag', 'txt_ptspc2');
set(h_obj,'String',num2str(pts_per_cycle));

%calculate # of Cycles and update field
num_cycles = pts / pts_per_cycle;
h_obj = findobj(gcf, 'Tag', 'txt_cycles2');
set(h_obj,'String',num2str(num_cycles));


function int_write_file()
h_obj = findobj(gcf, 'Tag', 'edit_out_freq');
str_fout = get(h_obj,'String');
fout = str2num(str_fout);

h_obj = findobj(gcf, 'Tag', 'edit_out_freq2');
str_fout2 = get(h_obj,'String');
fout2 = str2num(str_fout2);

h_obj = findobj(gcf, 'Tag', 'edit_sample_freq');
str_fs = get(h_obj,'String');
fs = str2num(str_fs);

h_obj = findobj(gcf, 'Tag', 'edit_data_pts');
str_data_pts = get(h_obj,'String');
data_pts = str2num(str_data_pts);

h_obj = findobj(gcf, 'Tag', 'edit_bit_width');
str_bit_width = get(h_obj,'String');
bit_width = str2num(str_bit_width);

h_obj = findobj(gcf, 'Tag', 'rbtn_sin');
val_sin = get(h_obj,'Value');

h_obj = findobj(gcf, 'Tag', 'rbtn_cos');
val_cos = get(h_obj,'Value');

h_obj = findobj(gcf, 'Tag', 'rbtn_sin2');
val_sin2 = get(h_obj,'Value');

h_obj = findobj(gcf, 'Tag', 'rbtn_cos2');
val_cos2 = get(h_obj,'Value');

h_obj = findobj(gcf, 'Tag', 'rbtn_dec');
val_dec = get(h_obj,'Value');

h_obj = findobj(gcf, 'Tag', 'rbtn_hex');
val_hex = get(h_obj,'Value');

h_obj = findobj(gcf, 'Tag', 'chbox_multi_wave');
val_chkbox_multi_wave = get(h_obj,'Value');

if (val_sin == 1)
  radians = 'sin';
elseif (val_cos == 1)
  radians = 'cos';
end

if (val_sin2 == 1)
  radians2 = 'sin';
elseif (val_cos2 == 1)
  radians2 = 'cos';
end

if (val_dec == 1)
  radix = 'dec';
elseif (val_hex == 1)
  radix = 'hex';
end

if (val_chkbox_multi_wave == 0)
    filename = [str_bit_width, 'bit_', radians, str_fout, 'mhz_fs', str_fs, 'mhz_', radix '.dat'];
else
    filename = [str_bit_width, 'bit_', radians, str_fout, '+', radians2, str_fout2, 'mhz_fs', str_fs, 'mhz_', radix '.dat'];
end

t = 0:1/fs:(data_pts)/fs;
if (radians == 'sin')
  s = sin (2*pi*fout*t);
elseif (radians == 'cos')
  s = cos (2*pi*fout*t);
end

t = 0:1/fs:(data_pts)/fs;
if (radians2 == 'sin')
  s2 = sin (2*pi*fout2*t);
elseif (radians2 == 'cos')
  s2 = cos (2*pi*fout2*t);
end
    
if (val_chkbox_multi_wave == 0)    
    sq = round(s * ((2^(bit_width-1))-1))';
else
    sq = round((s+s2) * ((2^(bit_width-2))-1))';
end

fid=fopen(filename, 'wt');
for i = 1:length(sq)
  if (radix == 'dec')
    fprintf(fid, '%i\n', sq(i));
  elseif (radix == 'hex')
    display('Hex Format not yet supported')
%    fprintf(fid, '%x\n', sq(i));
    break;
  end
end
fclose(fid);



function edit_out_freq2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_out_freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_out_freq2 as text
%        str2double(get(hObject,'String')) returns contents of edit_out_freq2 as a double
str_out_freq = get(hObject,'String');
if (length(str_out_freq) == 0)
  set(hObject,'String','1');
end
int_update_information2;

% --- Executes during object creation, after setting all properties.
function edit_out_freq2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_out_freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit_sample_freq2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_sample_freq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in rbtn_sin2.
function rbtn_sin2_Callback(hObject, eventdata, handles)
% hObject    handle to rbtn_sin2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtn_sin2
val_sin = get(hObject, 'Value');
if (val_sin == 1)
  h_obj = findobj(gcf, 'Tag', 'rbtn_cos2');
  set(h_obj,'Value', 0);
end

% --- Executes on button press in rbtn_cos2.
function rbtn_cos2_Callback(hObject, eventdata, handles)
% hObject    handle to rbtn_cos2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbtn_cos2
val_cos = get(hObject, 'Value');
if (val_cos == 1)
  h_obj = findobj(gcf, 'Tag', 'rbtn_sin2');
  set(h_obj,'Value', 0);
end

% --- Executes on button press in chbox_multi_wave.
function chbox_multi_wave_Callback(hObject, eventdata, handles)
% hObject    handle to chbox_multi_wave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chbox_multi_wave


