function [filedirs,filelines] = filegrep(varargin)
% [FILEDIRS, FILELINES] = FILEGREP
% This function should be called without input arguments; it launches a GUI window that initiates a GREP-type search
% of the contents of files of different types for the inclusion/exclusion of key words or phrases. Alternatively, files
% may be searched by filename only, without regard to the contents of the file. Inclusions are defined by the user; the
% default is '*.*', with the important caveat that, unless the "Filenames Only" option is checked, filetypes excluded
% by the default "SmartSearch" option are excluded.
%
% NOTES: Function FILEGREP incorporates as a subfunction a modified version of TMW's GENPATH.
% The unmodified version skips 'private' and 'class' directories; the subfunction version includes them.
% 
% Written by Brett Shoelson, Ph.D.
% shoelson@helix.nih.gov
% Version 1 release: 01/20/04
%
% Modified by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

% Initialize some variables
global done
filedirs = []; grepopts.filedirs = filedirs; filelines = [];datelist = [];sizelist = [];extensionlist = [];
if nargin == 0
	prevwords = ' ';
	prevdirectory = [];
else
	grepopts = varargin{1};
	prevwords = get(grepopts.allwordspopup,'string');
	tmp = get(grepopts.allwordsbox,'string');
	prevwords = unique(strvcat(prevwords,tmp),'rows');
	tmp = get(grepopts.mustincludepopup,'string');
	tmp2 = get(grepopts.mustincludebox,'string');
	prevwords = unique(strvcat(prevwords,tmp,tmp2),'rows');
	tmp = get(grepopts.somewordspopup,'string');
	tmp2 = get(grepopts.somewordsbox,'string');
	prevwords = unique(strvcat(prevwords,tmp,tmp2),'rows');
	tmp = get(grepopts.mustexcludepopup,'string');
	tmp2 = get(grepopts.mustexcludebox,'string');
	prevwords = unique(strvcat(prevwords,tmp,tmp2),'rows');
	prevdirectory = get(grepopts.usedirbox,'string');
end
grepopts.rel = ver; grepopts.rel = grepopts.rel(1).Release; grepopts.rel = str2num(grepopts.rel(3:end-1));
grepopts.overrideexe = 0;
alphachars = ['ABCDEFGHIJKLMNOPQRSTUVWXYZ']';
alphachars = [alphachars; lower(alphachars); ['1234567890']'];
allwords = '';
mustinclude = '';
mustexclude = '';

% Close figures from previous search
close(findobj('name', 'FileGrep'));
close(findobj('name', 'FileGrep Results'));
done = 0;

% Create new figure and store function handles
h_fig = figure('units', 'normalized', 'numbertitle', 'off','position', [0.02 0.05 0.96 0.9], ...%, 'resize', 'off'
	'name', 'FileGrep', 'color', [0.16 0.62 0.63], 'menubar', 'none', 'tag', 'h_fig',...
	'closerequestfcn','global done; done=1; delete(gcf)');
setappdata(h_fig, 'adddirectoryfcnhandle', @adddirectoryfcn);
setappdata(h_fig, 'removedirectoryfcnhandle', @removedirectoryfcn);
setappdata(h_fig, 'moveleftfcnhandle', @moveleftfcn);
setappdata(h_fig, 'moverightfcnhandle', @moverightfcn);
setappdata(h_fig, 'moveextleftfcnhandle', @moveextleftfcn);
setappdata(h_fig, 'moveextrightfcnhandle', @moveextrightfcn);
setappdata(h_fig, 'selallleftfcnhandle', @selallleftfcn);
setappdata(h_fig, 'selallrightfcnhandle', @selallrightfcn);
setappdata(h_fig, 'keepselectedleftfcnhandle', @keepselectedleftfcn);
setappdata(h_fig, 'clearleftfcnhandle', @clearleftfcn);
setappdata(h_fig, 'keepselectedrightfcnhandle', @keepselectedrightfcn);
setappdata(h_fig, 'clearrightfcnhandle', @clearrightfcn);
setappdata(h_fig, 'addextfcnhandle', @addext);
setappdata(h_fig, 'monlyfcnhandle', @monlyfcn);
setappdata(h_fig, 'txtonlyfcnhandle', @txtonlyfcn);
setappdata(h_fig, 'editselectedfcnhandle', @editselectedfcn);
setappdata(h_fig, 'hideselectedfcnhandle', @hideselectedfcn);
setappdata(h_fig, 'keepselectedfcnhandle', @keepselectedfcn);
setappdata(h_fig, 'deleteselectedfcnhandle', @deleteselectedfcn);
setappdata(h_fig, 'replacestringfcnhandle', @replacestringfcn);
setappdata(h_fig, 'savefilefcnhandle', @savefilefcn);
setappdata(h_fig, 'commentsonlyfcnhandle', @commentsonlyfcn);
setappdata(h_fig, 'excludecommentsfcnhandle', @excludecommentsfcn);
setappdata(h_fig, 'exclusionsfcnhandle', @exclusionsfcn);
setappdata(h_fig, 'todayonlyfcnhandle', @todayonlyfcn);
setappdata(h_fig, 'datelimitfcnhandle', @datelimitfcn);
setappdata(h_fig, 'dateminfcnhandle', @dateminfcn);
setappdata(h_fig, 'datemaxfcnhandle', @datemaxfcn);
setappdata(h_fig, 'sizelimitfcnhandle', @sizelimitfcn);
setappdata(h_fig, 'sizeminfcnhandle', @sizeminfcn);
setappdata(h_fig, 'sizemaxfcnhandle', @sizemaxfcn);
setappdata(h_fig, 'ignoredatesfcnhandle', @ignoredatesfcn);
setappdata(h_fig, 'usedatesfcnhandle', @usedatesfcn);
setappdata(h_fig, 'ignoresizesfcnhandle', @ignoresizesfcn);
setappdata(h_fig, 'usesizesfcnhandle', @usesizesfcn);
setappdata(h_fig, 'usekbfcnhandle', @usekbfcn);
setappdata(h_fig, 'usembfcnhandle', @usembfcn);
setappdata(h_fig, 'checkvaluesfcnhandle', @checkvaluesfcn);
setappdata(h_fig, 'popallwordsfcnhandle', @popallwordsfcn);
setappdata(h_fig, 'popmustincludefcnhandle', @popmustincludefcn);
setappdata(h_fig, 'popsomewordsfcnhandle', @popsomewordsfcn);
setappdata(h_fig, 'popmustexcludefcnhandle', @popmustexcludefcn);
setappdata(h_fig, 'sortfcnhandle', @sortfcn);
setappdata(h_fig, 'changeselectionfcnhandle', @changeselectionfcn);
setappdata(h_fig, 'scrollresultsfcnhandle', @scrollresultsfcn);
setappdata(h_fig, 'filenamesonlyfcnhandle', @filenamesonlyfcn);

% Create frame object that covers entire figure region.
h_frame = uicontrol(h_fig, 'style', 'frame', 'units', 'normalized', 'position', [0.02 0.02 0.96 0.96],...
	'BackgroundColor', [0.26 0.72 0.73],'tag','xxx');

%FRAME1
grepopts.frame1 = uicontrol(h_fig, 'style', 'frame', 'units', 'normalized', 'position', [0.04 0.58 0.92 0.37], ...
	'foregroundcolor', 'r', 'tag', 'frame1');

h_stext1 = uicontrol(h_fig, 'style', 'text', 'string', 'DIRECTORY OPTIONS', ...
	'fontsize', 10, 'fontname', 'Helvetica', 'units', 'normalized', 'position', [0.1 0.95 0.8 0.0275], 'foregroundcolor', 'b', ...
	'BackgroundColor', [0.26 0.72 0.73], 'HorizontalAlignment', 'Center');

msgstr = sprintf('+ : recursive search        - : non-recursive search.');
uicontrol(h_fig, 'style', 'text', 'string', msgstr, 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.06 0.8725 0.3 0.0685], 'HorizontalAlignment', 'center','tag','frame1');
if grepopts.rel < 13
	msgstr = 'Add directory to search path by selecting a file';
else
	msgstr = 'Add directory to search path';
end
grepopts.adddir = uicontrol(h_fig,'style','pushbutton','units','normalized','position', [0.06 0.88 0.3 0.03],'string', msgstr,...
	'callback','feval(getappdata(gcf,''adddirectoryfcnhandle''));',...
	'horizontalalignment','center','fontweight','b','tag','frame1','foregroundcolor','r');
grepopts.usedirbox = uicontrol(h_fig,'style','listbox','units','normalized','Position', [0.06 0.67 0.3 0.2],...
	'Max',10,'Min',1,'tag','frame1','fontname','courier new','fontsize',9,'string',prevdirectory);
uicontrol(h_fig,'style','pushbutton','units','normalized','position', [0.06 0.63 0.3 0.03],'string', 'Remove selected directory(ies)',...
	'callback','feval(getappdata(gcf,''removedirectoryfcnhandle''));',...
	'horizontalalignment','center','fontweight','b','tag','frame1');

%Select Extensions
msgstr = sprintf('Default search includes filenames of all extensions not excluded by "Smart Search"\nor "Ignore Hidden filenames." Use the left and right arrow buttons to refine selection.');
uicontrol(h_fig, 'style', 'text', 'string', msgstr, 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.41 0.8725 0.5 0.0685], 'HorizontalAlignment', 'Left','tag','frame1');
uicontrol(h_fig, 'style', 'text', 'string', 'EXCLUDE EXTENSIONS:', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.41 0.865 0.2 0.03], 'HorizontalAlignment', 'Left', 'fontweight', 'bold','tag','frame1');
uicontrol(h_fig, 'style', 'text', 'string', 'INCLUDE EXTENSIONS:', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.625 0.865 0.2 0.03], 'HorizontalAlignment', 'Left', 'fontweight', 'bold','tag','frame1');
grepopts.excludebox = uicontrol(h_fig,'style','listbox','units','normalized','Position', [0.41 0.67 0.125 0.2],...
	'Max',10,'Min',1,'tag','frame1','fontsize',12);
grepopts.extensionsbox = uicontrol(h_fig,'style','listbox','units','normalized','Position', [0.625 0.67 0.125 0.2],...
	'Max',10,'Min',1,'tag','frame1','fontsize',12);
%DEFAULT BEHAVIOR:
set(grepopts.extensionsbox,'value',[],'string','');

uicontrol(h_fig,'style','pushbutton','units','normalized','string','<','position',[0.555 0.83 0.05 0.04],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''moveleftfcnhandle''));','tooltipstring','Shift selected items to the left.');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','>','position',[0.555 0.785 0.05 0.04],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''moverightfcnhandle''));','tooltipstring','Shift selected items to the right.');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','Select/Move All','position',[0.41 0.63 0.125 0.03],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''selallleftfcnhandle''));','tooltipstring','Shift all items to the right.');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','Keep Selected','position',[0.41 0.595 0.1 0.03],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''keepselectedleftfcnhandle''));','tooltipstring','Shift non-selected items to the right.');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','X','position',[0.51 0.595 0.025 0.03],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''clearleftfcnhandle''));','tooltipstring','Clear "Exclusions" box.');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','Select/Move All','position',[0.625 0.63 0.125 0.03],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''selallrightfcnhandle''));','tooltipstring','Shift all items to the left.');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','Keep Selected','position',[0.625 0.595 0.1 0.03],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''keepselectedrightfcnhandle''));','tooltipstring','Shift non-selected items to the left.');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','X','position',[0.725 0.595 0.025 0.03],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''clearrightfcnhandle''));','tooltipstring','Clear "Extensions" box.');
uicontrol(h_fig, 'style', 'text', 'string', 'Add/Rem Extension', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.55 0.715 0.06 0.04], 'HorizontalAlignment', 'Center','tag','frame1');
grepopts.sel_ext = uicontrol(h_fig,'style','edit','units','normalized','position',[0.555 0.67 0.05 0.04],'callback','','tag','frame1');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','<','position',[0.555 0.625 0.025 0.04],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''moveextleftfcnhandle''));','tooltipstring','Add entry to the "Exclusions" box.');
uicontrol(h_fig,'style','pushbutton','units','normalized','string','>','position',[0.58 0.625 0.025 0.04],...
	'fontweight','b','tag','frame1',...
	'callback','feval(getappdata(gcf,''moveextrightfcnhandle''));','tooltipstring','Add entry to the "Extensions" box.');

%.txt
uicontrol(h_fig,'style','pushbutton','units','normalized','string','','position',[0.765 0.85 0.015 0.02],...
	'tag','frame1','callback','feval(getappdata(gcf,''addextfcnhandle''),''.txt'');');
uicontrol(h_fig, 'style', 'text', 'string','.txt', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.785 0.85 0.03 0.02], 'HorizontalAlignment', 'Left','tag','frame2');
%.m
uicontrol(h_fig,'style','pushbutton','units','normalized','string','','position',[0.765 0.8275 0.015 0.02],...
	'tag','frame1','callback','feval(getappdata(gcf,''addextfcnhandle''),''.m'');');
uicontrol(h_fig, 'style', 'text', 'string','.m', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.785 0.8275 0.03 0.02], 'HorizontalAlignment', 'Left','tag','frame2');
%.xls
uicontrol(h_fig,'style','pushbutton','units','normalized','string','','position',[0.765 0.805 0.015 0.02],...
	'tag','frame1','callback','feval(getappdata(gcf,''addextfcnhandle''),''.xls'');');
uicontrol(h_fig, 'style', 'text', 'string','.xls', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.785 0.805 0.03 0.02], 'HorizontalAlignment', 'Left','tag','frame2');
%.doc
uicontrol(h_fig,'style','pushbutton','units','normalized','string','','position',[0.765 0.7825 0.015 0.02],...
	'tag','frame1','callback','feval(getappdata(gcf,''addextfcnhandle''),''.doc'');');
uicontrol(h_fig, 'style', 'text', 'string','.doc', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.785 0.7825 0.03 0.02], 'HorizontalAlignment', 'Left', 'tag','frame2');
%.*
uicontrol(h_fig,'style','pushbutton','units','normalized','string','','position',[0.765 0.76 0.015 0.02],...
	'tag','frame1','callback','feval(getappdata(gcf,''addextfcnhandle''),''.*'');',...
	'tooltipstring','Ignore filenames specified by "Exclude Extensions," "Smart Search," and "Hidden filenames." Search all others.');
uicontrol(h_fig, 'style', 'text', 'string','.* (All non-excluded filenames)', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.785 0.76 0.17 0.02], 'HorizontalAlignment', 'Left','tag','frame2',...
	'tooltipstring','Ignore filenames specified by "Exclude Extensions," "Smart Search," and "Hidden filenames." Search all others.');

grepopts.txtonly = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.765 0.7 0.175 0.03],...
	'string', 'Search ".txt" files only','value',0,'tag','frame1','fontsize',9,...
	'callback','feval(getappdata(gcf,''txtonlyfcnhandle''));');
grepopts.monly = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.765 0.67 0.175 0.03],...
	'string', 'Search ".m" files only','value',0,'tag','frame1','fontsize',9,...
	'callback','feval(getappdata(gcf,''monlyfcnhandle''));');

%FRAME2
uicontrol(h_fig, 'style', 'frame', 'units', 'normalized', 'position', [0.04 0.16 0.92 0.38],...
	'foregroundcolor', 'r','tag','frame2');

h_stext2 = uicontrol(h_fig, 'style', 'text', 'string', 'SEARCH/DISPLAY OPTIONS', ...
	'fontsize', 10, 'fontname', 'Helvetica', 'units', 'normalized', 'position', [0.1 0.5425 0.8 0.025], 'foregroundcolor', 'b', ...
	'BackgroundColor', [0.26 0.72 0.73], 'HorizontalAlignment', 'Center','tag','frame2');

%Search String
uicontrol(h_fig, 'style', 'text', 'string','With all of the words:', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.06 0.467 0.16 0.04], 'HorizontalAlignment', 'Left', 'fontweight', 'bold','tag','frame2');
grepopts.allwordspopup = uicontrol(h_fig, 'style', 'popup', 'string', prevwords, 'units', 'normalized',...
	'position',[0.215 0.485 0.265 0.03] , 'fontname', 'Helvetica', 'fontsize', 7,...
	'HorizontalAlignment', 'Left','tag','frame2',...
	'callback','feval(getappdata(gcf,''popallwordsfcnhandle''));');
grepopts.allwordsbox = uicontrol(h_fig, 'style', 'edit', 'string', '', 'units', 'normalized',...
	'position', [0.215 0.485 0.245 0.03], 'fontname', 'Helvetica', 'fontsize', 8, 'HorizontalAlignment', 'Left','tag','frame2',...
	'tooltipstring','Enter all words that MUST be in the file. Do not enter quotation marks or delimiters.',...
	'callback','feval(getappdata(gcf,''checkvaluesfcnhandle''));');
uicontrol(h_fig, 'style', 'text', 'string', 'With the exact phrase:', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.06 0.427 0.16 0.04], 'HorizontalAlignment', 'Left', 'fontweight', 'bold','tag','frame2');
msgstr = sprintf('Search for exact phrase(s).\nEnclose multiple phrases in quotation marks. (Single phrases need not be enclosed in quotations marks.)');
grepopts.mustincludepopup = uicontrol(h_fig, 'style', 'popup', 'string', prevwords, 'units', 'normalized',...
	'position', [0.215 0.445 0.265 0.03], 'fontname', 'Helvetica', 'fontsize', 7,...
	'HorizontalAlignment', 'Left','tag','frame2',...
	'callback','feval(getappdata(gcf,''popmustincludefcnhandle''));');
grepopts.mustincludebox = uicontrol(h_fig, 'style', 'edit', 'string', '', 'units', 'normalized',...
	'position', [0.215 0.445 0.245 0.03], 'fontname', 'Helvetica', 'fontsize', 8, 'HorizontalAlignment', 'Left','tag','frame2',...
	'tooltipstring',msgstr,'callback','feval(getappdata(gcf,''checkvaluesfcnhandle''));');
uicontrol(h_fig, 'style', 'text', 'string', 'With any of the words:', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.5 0.467 0.16 0.04], 'HorizontalAlignment', 'Left', 'fontweight', 'bold','tag','frame2');
grepopts.somewordspopup = uicontrol(h_fig, 'style', 'popup', 'string', prevwords, 'units', 'normalized',...
	'position', [0.675 0.485 0.265 0.03], 'fontname', 'Helvetica', 'fontsize', 7,...
	'HorizontalAlignment', 'Left','tag','frame2',...
	'callback','feval(getappdata(gcf,''popsomewordsfcnhandle''));');
grepopts.somewordsbox = uicontrol(h_fig, 'style', 'edit', 'string', '', 'units', 'normalized',...
	'position', [0.675 0.485 0.245 0.03], 'fontname', 'Helvetica', 'fontsize', 8, 'HorizontalAlignment', 'Left','tag','frame2',...
	'callback','feval(getappdata(gcf,''checkvaluesfcnhandle''));');
uicontrol(h_fig, 'style', 'text', 'string', 'With none of the words:', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.5 0.427 0.16 0.04], 'HorizontalAlignment', 'Left', 'fontweight', 'bold','tag','frame2');
grepopts.mustexcludepopup = uicontrol(h_fig, 'style', 'popup', 'string', prevwords, 'units', 'normalized',...
	'position', [0.675 0.445 0.265 0.03], 'fontname', 'Helvetica', 'fontsize', 7,...
	'HorizontalAlignment', 'Left','tag','frame2',...
	'callback','feval(getappdata(gcf,''popmustexcludefcnhandle''));');
grepopts.mustexcludebox = uicontrol(h_fig, 'style', 'edit', 'string', '', 'units', 'normalized',...
	'position', [0.675 0.445 0.245 0.03], 'fontname', 'Helvetica', 'fontsize', 8, 'HorizontalAlignment', 'Left','tag','frame2',...
	'callback','feval(getappdata(gcf,''checkvaluesfcnhandle''));');
grepopts.currbox = grepopts.mustexcludebox;

uicontrol(h_fig, 'style', 'text', 'string',...
	'OPTIONS ____________________________________________________________________________________________________________', ...
	'fontsize', 9, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.06 0.4 0.88 0.03], 'HorizontalAlignment', 'Left', 'fontweight', 'bold','tag','frame2');
grepopts.wholewords = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.06 0.37 0.3 0.03],...
	'string', 'Whole words','value',0,'tag','frame2');
grepopts.casesensitive = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.06 0.34 0.3 0.03],...
	'string', 'Case sensitive','value',0,'tag','frame2');
grepopts.excludecomments = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.06 0.31 0.2 0.03],...
	'string', 'Exclude commented lines','value',0,'tag','frame2',...
	'callback','feval(getappdata(gcf,''excludecommentsfcnhandle''));',...
	'tooltipstring','Ignore lines preceded (as a first non-blank) by the "Comment Character" indicated below.');
grepopts.commentsonly = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.06 0.28 0.3 0.03],...
	'string', 'Search comments only','value',0,'tag','frame2',...
	'callback','feval(getappdata(gcf,''commentsonlyfcnhandle''));',...
	'tooltipstring','Consider only lines preceded (as a first non-blank) by the "Comment Character" indicated below.');
uicontrol(h_fig, 'style', 'text', 'string', 'Comment Character:', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.08 0.246 0.2 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2');
grepopts.commentchar = uicontrol(h_fig,'style','edit','units','normalized','position',[0.2 0.25 0.025 0.0275],...
	'string','%','tag','frame2','enable','off');
grepopts.ignorehidden = uicontrol(h_fig, 'style', 'checkbox', 'string', 'Ignore hidden files', 'fontsize', 8, ...
	'fontname', 'Helvetica', 'units', 'normalized', 'value',1,...
	'position', [0.06 0.22 0.2 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2',...
	'callback',...
	['if ~get(gcbo,''value'');',...
		'overrideexe = questdlg(''By default, filetypes *.exe, *.ini, *.drv, *.sys, and *.dll are ignored regardless of the status of the "Ignore Hidden Files" option. Do you want to override this behavior and include these files in your search?'',''Ignore executables, DLLs, etc?'',''NO'',''Yes, override defaults'',''NO'');',...
		'grepopts = getappdata(gcf,''grepopts'');',...
		'if overrideexe,',...
		'grepopts.overrideexe = 1;',...
		'else;',...
		'grepopts.overrideexe = 0;',...
		'end;',...
		'setappdata(gcf,''grepopts'',grepopts);',...
		'end;',...
		'feval(getappdata(gcf,''exclusionsfcnhandle''));']);
grepopts.smartsearch = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.26 0.37 0.3 0.03],...
	'string', 'SmartSearch','value',1,'tag','frame2','tooltipstring',...
	sprintf('Ignore nonprinting and obscure ASCII characters.\nAlso ignores recognized video and image filenames, unless the "File names only" option is selected.'),...
	'callback','feval(getappdata(gcf,''exclusionsfcnhandle''));');
grepopts.filenamesonly = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.26 0.34 0.3 0.03],...
	'string', 'File names only','value',0,'tag','frame2','tooltipstring','Search only the names of filenames.','callback',...
	'feval(getappdata(gcf,''filenamesonlyfcnhandle''));');
msgstr = sprintf('Scans selected directories to compile a list of extensions prior to executing the search.\nThis will speed up the selection of filenames, and will be fast when the search path is relatively small.');
grepopts.prescreenextensions = uicontrol(h_fig,'style','checkbox','units','normalized','position',[0.26 0.31 0.3 0.03],...
	'string', 'Pre-screen extensions','value',0,'tag','frame2','tooltipstring',msgstr,...
	'callback','feval(getappdata(gcf,''exclusionsfcnhandle''));');

uicontrol(h_fig, 'style', 'text', 'string', 'Date modified?', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.46 0.37 0.1 0.03], 'fontweight','b', 'HorizontalAlignment', 'Left', 'tag', 'frame2');
grepopts.ignoredates = uicontrol(h_fig, 'style', 'radio', 'string', 'Ignore dates', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.46 0.345 0.1 0.03], 'value',1, 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','IgnoreDates',...
	'callback','feval(getappdata(gcf,''ignoredatesfcnhandle''));');
grepopts.modday = uicontrol(h_fig, 'style', 'radio', 'string', 'Today', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.48 0.315 0.1 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','dateradio',...
	'callback','feval(getappdata(gcf,''usedatesfcnhandle''));',...
	'tooltipstring','Search filenames modified today.');
grepopts.modmonth = uicontrol(h_fig, 'style', 'radio', 'string', 'This month', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.48 0.285 0.1 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','dateradio',...
	'callback','feval(getappdata(gcf,''usedatesfcnhandle''));',...
	'tooltipstring','Search filenames modified in the past 30 days.');
grepopts.modyear = uicontrol(h_fig, 'style', 'radio', 'string', 'This year', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.48 0.255 0.1 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','dateradio',...
	'callback','feval(getappdata(gcf,''usedatesfcnhandle''));',...
	'tooltipstring','Search filenames modified in the past 12 months.');
grepopts.datelimit = uicontrol(h_fig, 'style', 'radio', 'string', 'Specify dates', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.48 0.225 0.2 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','dateradio',...
	'callback','feval(getappdata(gcf,''datelimitfcnhandle''));');
uicontrol(h_fig, 'style', 'text', 'string', 'Between', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.5 0.2 0.05 0.022], 'HorizontalAlignment', 'Right', 'tag', 'frame2');
grepopts.datemin = uicontrol(h_fig,'style','edit','units','normalized','position',[0.565 0.199 0.1 0.0275],...
	'string', datestr(datenum(now)-365,1),'value',0,'tag','frame2','enable','off',...
	'callback','feval(getappdata(gcf,''dateminfcnhandle''));');
uicontrol(h_fig, 'style', 'text', 'string', 'and', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.5 0.17 0.05 0.022], 'HorizontalAlignment', 'Right', 'tag', 'frame2');
grepopts.datemax = uicontrol(h_fig,'style','edit','units','normalized','position',[0.565 0.169 0.1 0.0275],...
	'string', date,'value',0,'tag','frame2','enable','off',...
	'callback','feval(getappdata(gcf,''datemaxfcnhandle''));');

uicontrol(h_fig, 'style', 'text', 'string', 'File size?', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.69 0.37 0.1 0.03], 'fontweight','b', 'HorizontalAlignment', 'Left', 'tag', 'frame2');
grepopts.ignoresizes = uicontrol(h_fig, 'style', 'radio', 'string', 'Ignore size',...
	'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.69 0.345 0.1 0.03], 'value',1, 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','IgnoreSizes',...
	'callback','feval(getappdata(gcf,''ignoresizesfcnhandle''));');
grepopts.smallfiles = uicontrol(h_fig, 'style', 'radio', 'string', 'Small (< 100 KB)', 'fontsize', 8, 'fontname', ...
	'Helvetica', 'units', 'normalized', 'position', [0.71 0.315 0.12 0.03], 'HorizontalAlignment', 'Left',...
	'tag', 'frame2','userdata','sizeradio','value', 0,...
	'callback','feval(getappdata(gcf,''usesizesfcnhandle''));','tooltipstring','DEFAULT: Search small files only.');
grepopts.mediumfiles = uicontrol(h_fig, 'style', 'radio', 'string', 'Medium (< 1 MB)', 'fontsize', 8, ...
	'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.71 0.285 0.12 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','sizeradio',...
	'callback','feval(getappdata(gcf,''usesizesfcnhandle''));');
grepopts.largefiles = uicontrol(h_fig, 'style', 'radio', 'string', 'Large (> 1 MB)', 'fontsize', 8, ...
	'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.71 0.255 0.12 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','sizeradio',...
	'callback','feval(getappdata(gcf,''usesizesfcnhandle''));');
grepopts.sizelimit = uicontrol(h_fig, 'style', 'radio', 'string', 'Specify file sizes', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.71 0.225 0.2 0.03], 'HorizontalAlignment', 'Left', 'tag', 'frame2','userdata','sizeradio',...
	'callback','feval(getappdata(gcf,''sizelimitfcnhandle''));');
uicontrol(h_fig, 'style', 'text', 'string', 'Between', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.73 0.2 0.05 0.022], 'HorizontalAlignment', 'Left', 'tag', 'frame2');
grepopts.sizemin = uicontrol(h_fig,'style','edit','units','normalized','position',[0.795 0.199 0.1 0.0275],...
	'string', 0,'value',0,'tag','frame2','enable','off',...
	'callback','feval(getappdata(gcf,''sizeminfcnhandle''));');
uicontrol(h_fig, 'style', 'text', 'string', 'and', 'fontsize', 8, 'fontname', 'Helvetica', 'units', 'normalized', ...
	'position', [0.73 0.17 0.05 0.022], 'HorizontalAlignment', 'Right', 'tag', 'frame2');
grepopts.sizemax = uicontrol(h_fig,'style','edit','units','normalized','position',[0.795 0.169 0.1 0.0275],...
	'string', 50,'value',0,'tag','frame2','enable','off',...
	'callback','feval(getappdata(gcf,''sizemaxfcnhandle''));');
grepopts.usekb = uicontrol(h_fig, 'style', 'radio', 'string', 'KB', 'fontsize', 8, ...
	'fontname', 'Helvetica', 'units', 'normalized', 'value', 1,'enable','off',...
	'position', [0.91 0.2 0.04 0.022], 'HorizontalAlignment', 'Left', 'tag', 'frame2',...
	'callback','feval(getappdata(gcf,''usekbfcnhandle''));');
grepopts.usemb = uicontrol(h_fig, 'style', 'radio', 'string', 'MB', 'fontsize', 8, ...
	'fontname', 'Helvetica', 'units', 'normalized', 'enable','off',...
	'position', [0.91 0.17 0.04 0.022], 'HorizontalAlignment', 'Left', 'tag', 'frame2',...
	'callback','feval(getappdata(gcf,''usembfcnhandle''));');

%FRAME3
uicontrol(h_fig, 'style', 'frame', 'units', 'normalized', 'position', [0.04 0.035 0.7 0.075],...
	'foregroundcolor', 'r','tag','frame3');

uicontrol(h_fig, 'style', 'text', 'string', 'STATUS...', ...
	'fontsize', 10, 'fontname', 'Helvetica', 'units', 'normalized', 'position', [0.05 0.1125 0.68 0.025], 'foregroundcolor', 'b', ...
	'BackgroundColor', [0.26 0.72 0.73], 'HorizontalAlignment', 'Center');

grepopts.status = uicontrol(h_fig,'style','text','string','',...
	'units','normalized','position',[0.05 0.036 0.68 0.055],'fontsize',8,'fontweight','b','foregroundcolor','k',...
	'horizontalalignment','center');

%RESULTS LISTBOXES
grepopts.selectallbutton = uicontrol(h_fig,'style','pushbutton','string','�','fontname','wingdings','tooltipstring','Toggle all...','units','normalized','position',[0.028 0.31 0.012 0.015],...
	'visible','off','tag','results','callback',...
	['grepopts = getappdata(gcf,''grepopts'');',...
		'if isempty(get(grepopts.resultsbox,''value''));',...
		'tmp = 1:length(get(grepopts.resultsbox,''string''))-1;',...
		'else;',...
		'tmp = [];',...
		'end;',...
		'set(grepopts.resultsbox,''value'',tmp);',...
		'set(grepopts.stringbox,''value'',tmp);',...
		'set(grepopts.datebox,''value'',tmp);',...
		'set(grepopts.extensionbox,''value'',tmp);',...
		'set(grepopts.sizebox,''value'',tmp);',...
		'feval(getappdata(gcf,''changeselectionfcnhandle''),''results'');']);
grepopts.resultsbox = uicontrol(h_fig,'style','listbox','units','normalized','Position', [0.04 0.31 0.28 0.62],...
	'Max',10,'Min',1,'tag','results','visible','off',...
	'callback','feval(getappdata(gcf,''changeselectionfcnhandle''),''results'');');
grepopts.sortresultsbutton = uicontrol('style','pushbutton','string','Filename','fontsize',10,'units','normalized',...
	'position',[0.04 0.93 0.262 0.03],'tag','results','visible','off','callback','feval(getappdata(gcf,''sortfcnhandle''),''results'');',...
	'SelectionHighlight','off');
grepopts.stringbox = uicontrol(h_fig,'style','listbox','units','normalized','Position', [0.3 0.31 0.42 0.62],...
	'Max',10,'Min',1,'tag','results','visible','off',...
	'callback','feval(getappdata(gcf,''changeselectionfcnhandle''),''string'');');
grepopts.sortstringbutton = uicontrol('style','pushbutton','string','String','fontsize',10,'units','normalized',...
	'position',[0.302 0.93 0.4 0.03],'tag','results','visible','off','callback','feval(getappdata(gcf,''sortfcnhandle''),''string'');',...
	'SelectionHighlight','off');
grepopts.datebox = uicontrol(h_fig,'style','listbox','units','normalized','Position', [0.7 0.31 0.15 0.62],...
	'Max',10,'Min',1,'tag','results','visible','off',...
	'callback','feval(getappdata(gcf,''changeselectionfcnhandle''),''date'');');
grepopts.sortdatebutton = uicontrol('style','pushbutton','string','Date','fontsize',10,'units','normalized',...
	'position',[0.702 0.93 0.13 0.03],'tag','results','visible','off','callback','feval(getappdata(gcf,''sortfcnhandle''),''date'');',...
	'SelectionHighlight','off');
grepopts.extensionbox = uicontrol(h_fig,'style','listbox','units','normalized','Position', [0.83 0.31 0.075 0.62],...
	'Max',10,'Min',1,'tag','results','visible','off',...
	'callback','feval(getappdata(gcf,''changeselectionfcnhandle''),''extension'');');
grepopts.sortextensionbutton = uicontrol('style','pushbutton','string','Ext','fontsize',10,'units','normalized',...
	'position',[0.832 0.93 0.055 0.03],'tag','results','visible','off','callback','feval(getappdata(gcf,''sortfcnhandle''),''extension'');',...
	'SelectionHighlight','off');
grepopts.sizebox = uicontrol(h_fig,'style','listbox','units','normalized','Position', [0.885 0.31 0.075 0.62],...
	'Max',10,'Min',1,'tag','results','visible','off',...
	'callback','feval(getappdata(gcf,''changeselectionfcnhandle''),''size'');','value',10);
grepopts.sortsizebutton = uicontrol('style','pushbutton','string','Size (kB)','fontsize',10,'units','normalized',...
	'position',[0.887 0.93 0.073 0.03],'tag','results','visible','off','callback','feval(getappdata(gcf,''sortfcnhandle''),''size'');',...
	'SelectionHighlight','off');
grepopts.scrollresults = uicontrol(h_fig,'style','slider','units','normalized','position',[0.887+0.053 0.31 0.02 0.62-0.003],...
	'backgroundcolor',[0.8 0.8 0.8],'visible','off','min',0,'max',1,'tag','results',...
	'callback','feval(getappdata(gcf,''scrollresultsfcnhandle''));');
[objpos,objdim] = distribute(6,0.04,0.96,0.01);
grepopts.editselectedbutton = uicontrol(h_fig,'style','pushbutton','units','normalized','string','Launch/Edit Selected File(s)',...
	'position',[objpos(1) 0.265 objdim 0.04],...
	'fontweight','b','tag','Results','visible','off','enable','off','foregroundcolor','r',...
	'callback','feval(getappdata(gcf,''editselectedfcnhandle''));','tooltipstring',...
	'Tries to edit or launch selected file(s) using system associations.');
grepopts.hideselectedbutton = uicontrol(h_fig,'style','pushbutton','units','normalized','string','Hide Selected File(s)',...
	'position',[objpos(2) 0.265 objdim 0.04],...
	'fontweight','b','tag','Results','visible','off','enable','off','foregroundcolor','r',...
	'callback','feval(getappdata(gcf,''hideselectedfcnhandle''));');
grepopts.keepselectedbutton = uicontrol(h_fig,'style','pushbutton','units','normalized','string','Keep Selected File(s)',...
	'position',[objpos(3) 0.265 objdim 0.04],...
	'fontweight','b','tag','Results','visible','off','enable','off','foregroundcolor','r',...
	'callback','feval(getappdata(gcf,''keepselectedfcnhandle''));');
grepopts.deleteselectedbutton = uicontrol(h_fig,'style','pushbutton','units','normalized','string','Move or Delete Selected File(s)',...
	'position',[objpos(4) 0.265 objdim 0.04],...
	'fontweight','b','tag','Results','visible','off','enable','off','foregroundcolor','r',...
	'callback','feval(getappdata(gcf,''deleteselectedfcnhandle''));');
grepopts.replacestringbutton = uicontrol(h_fig,'style','pushbutton','units','normalized','string','Replace String in Selected File(s)',...
	'position',[objpos(5) 0.265 objdim 0.04],...
	'fontweight','b','tag','Results','visible','off','enable','off','foregroundcolor','r',...
	'callback','feval(getappdata(gcf,''replacestringfcnhandle''));');
grepopts.savefilebutton = uicontrol(h_fig,'style','pushbutton','units','normalized','string','Save/Backup Selected File(s)',...
	'position',[objpos(6) 0.265 objdim 0.04],...
	'fontweight','b','tag','Results','visible','off','enable','off','foregroundcolor','r',...
	'callback','feval(getappdata(gcf,''savefilefcnhandle''));');

%SHOWPARAMETERS CONTROLS
uicontrol('style','text','units','normalized','position',[0.04 0.23 0.23 0.03],'string','WITH ALL WORDS:','tag','showparameters',...
	'backgroundcolor',[0.26 0.72 0.73],'visible','off','fontsize',8,'horizontalalignment','left');
grepopts.allworsdparam = uicontrol('style','edit','units','normalized','position',[0.04 0.205 0.23 0.03],'string','','tag','showparameters',...
	'visible','off','horizontalalignment','left','fontsize',8,'enable','inactive');
uicontrol('style','text','units','normalized','position',[0.04 0.17 0.23 0.03],'string','WITH EXACT PHRASE:','tag','showparameters',...
	'backgroundcolor',[0.26 0.72 0.73],'visible','off','fontsize',8,'horizontalalignment','left');
grepopts.exactphraseparam = uicontrol('style','edit','units','normalized','position',[0.04 0.145 0.23 0.03],'string','','tag','showparameters',...
	'visible','off','horizontalalignment','left','fontsize',8,'enable','inactive');
uicontrol('style','text','units','normalized','position',[0.285 0.23 0.23 0.03],'string','WITH ANY WORDS:','tag','showparameters',...
	'backgroundcolor',[0.26 0.72 0.73],'visible','off','fontsize',8,'horizontalalignment','left');
grepopts.somewordsparam = uicontrol('style','edit','units','normalized','position',[0.285 0.205 0.23 0.03],'string','','tag','showparameters',...
	'visible','off','horizontalalignment','left','fontsize',8,'enable','inactive');
uicontrol('style','text','units','normalized','position',[0.285 0.17 0.23 0.03],'string','EXCLUDING WORDS:','tag','showparameters',...
	'backgroundcolor',[0.26 0.72 0.73],'visible','off','fontsize',8,'horizontalalignment','left');
grepopts.mustexcludeparam = uicontrol('style','edit','units','normalized','position',[0.285 0.145 0.23 0.03],'string','','tag','showparameters',...
	'visible','off','horizontalalignment','left','fontsize',8,'enable','inactive');
grepopts.wholewordsparam = uicontrol('style','checkbox','units','normalized','position',[0.55 0.225 0.135 0.02],...
	'string','Whole words','tag','showparameters','visible','off','backgroundcolor',[0.26 0.72 0.73],'enable','inactive');
grepopts.casesensitiveparam = uicontrol('style','checkbox','units','normalized','position',[0.55 0.1975 0.135 0.02],...
	'string','Case Sensitive','tag','showparameters','visible','off','backgroundcolor',[0.26 0.72 0.73],'enable','inactive');
grepopts.excludecommentedparam = uicontrol('style','checkbox','units','normalized','position',[0.55 0.17 0.135 0.02],...
	'string','Exclude Commented','tag','showparameters','visible','off','backgroundcolor',[0.26 0.72 0.73],'enable','inactive');
grepopts.commentsonlyparam = uicontrol('style','checkbox','units','normalized','position',[0.55 0.1425 0.135 0.02],...
	'string','Comments Only','tag','showparameters','visible','off','backgroundcolor',[0.26 0.72 0.73],'enable','inactive');
grepopts.ignorehiddenparam = uicontrol('style','checkbox','units','normalized','position',[0.685 0.225 0.135 0.02],...
	'string','Ignore Hidden','tag','showparameters','visible','off','backgroundcolor',[0.26 0.72 0.73],'enable','inactive');
grepopts.smartsearchparam = uicontrol('style','checkbox','units','normalized','position',[0.82 0.225 0.135 0.02],...
	'string','Smart Search','tag','showparameters','visible','off','backgroundcolor',[0.26 0.72 0.73],'enable','inactive');
grepopts.filenamesonlyparam = uicontrol('style','checkbox','units','normalized','position',[0.685 0.1975 0.135 0.02],...
	'string','Filenames Only','tag','showparameters','visible','off','backgroundcolor',[0.26 0.72 0.73],'enable','inactive');
uicontrol('style','text','units','normalized','position',[0.705-0.02 0.17 0.135 0.02],'string','Date Parameter:','tag','showparameters',...
	'backgroundcolor',[0.26 0.72 0.73],'visible','off','fontsize',8,'horizontalalignment','left');
grepopts.dateparam = uicontrol('style','edit','units','normalized','position',[0.805-0.02 0.17 0.17 0.03],...
	'string','','tag','showparameters','visible','off','enable','inactive');
uicontrol('style','text','units','normalized','position',[0.685 0.1425 0.135 0.02],'string','Filesize Parameter:','tag','showparameters',...
	'backgroundcolor',[0.26 0.72 0.73],'visible','off','fontsize',8,'horizontalalignment','left');
grepopts.filesizeparam = uicontrol('style','edit','units','normalized','position',[0.785 0.1375 0.17 0.03],...
	'string','','tag','showparameters','visible','off','enable','inactive');

%FINISH BUTTONS
grepopts.startsearch = uicontrol(h_fig, 'style', 'pushbutton', 'string', 'START SEARCH...', ...
	'callback', ['global done;', 'done = 1;'], 'units', 'normalized', 'position', [0.76 0.07 0.2 0.04], 'fontname', 'Helvetica', ...
	'fontsize', 12, 'foregroundcolor', 'k', 'fontweight','b','BackgroundColor', [0.92549 0.913725 0.847059]);
cancelit = uicontrol(h_fig, 'style', 'pushbutton', 'string', 'Cancel', ...
	'callback', ...
	['global done;',...
		'grepopts = getappdata(gcf,''grepopts'');',...
		'switch done;',...
		'case 0;',...
		'delete(gcf);',...
		'case 1;',...
		'done = 2;',...
		'[grepopts.filedirs, filelines] = filegrep(grepopts);',...
		'end;'],...
	'units', 'normalized', 'position', [0.76 0.035 0.2 0.03], 'fontname', 'Helvetica', ...
	'fontsize', 10, 'foregroundcolor', 'k', 'BackgroundColor', [0.92549 0.913725 0.847059], 'fontweight','b'); 

setappdata(h_fig, 'grepopts', grepopts);

while done == 0
	drawnow;
	if done == 2
		delete(gcf);
		return
	end
	try
		tmpstring = get(grepopts.usedirbox,'string');
	catch
		return;
	end
	if isempty(tmpstring)
		if done == 1
			tmp2 = get(grepopts.usedirbox,'backgroundcolor');
			set(grepopts.usedirbox,'backgroundcolor','r');
			set(grepopts.status,'string','No directory was selected!');
			pause(2);
			set(grepopts.status,'string','');
			done = 0;
			set(grepopts.usedirbox,'backgroundcolor',tmp2);
		end
		set(grepopts.adddir,'foregroundcolor','r');
		set(grepopts.startsearch,'foregroundcolor','k');
	else
		set(grepopts.adddir,'foregroundcolor','k');
	end
	if isempty(get(grepopts.extensionsbox,'string')) & ~get(grepopts.prescreenextensions,'value')
		if done == 1
			tmp2 = get(grepopts.extensionsbox,'backgroundcolor');
			set(grepopts.status,'string','Extensions box is empty!');
			set(grepopts.extensionsbox,'backgroundcolor','r');
			tmp = questdlg('Search all file types?','Extension box is empty...','CANCEL','Yes','CANCEL');
			if strcmp(tmp,'Yes')
				set(grepopts.extensionsbox,'string','.*','value',[]);
				exclusionsfcn;
			else
				set(grepopts.status,'string','');
				done = 0;
			end
			set(grepopts.extensionsbox,'backgroundcolor',tmp2);
		end
		set(grepopts.startsearch,'foregroundcolor','k');
	end
	if isempty(get(grepopts.mustincludebox,'string')) & isempty(get(grepopts.allwordsbox,'string')) & ...
			isempty(get(grepopts.somewordsbox,'string')) & ~get(grepopts.filenamesonly,'value') % & isempty(get(grepopts.mustexcludebox,'string'))
		if done == 1
			tmp = get(grepopts.mustincludebox,'backgroundcolor');
			set(grepopts.status,'string',sprintf('No search string specified!\n(You must INCLUDE at least one word/phrase in your search.)'));
			set(grepopts.mustincludebox,'backgroundcolor','r');
			set(grepopts.allwordsbox,'backgroundcolor','r');
			set(grepopts.somewordsbox,'backgroundcolor','r');
			pause(2);
			set(grepopts.status,'string','');
			set(grepopts.mustincludebox,'backgroundcolor',tmp);
			set(grepopts.allwordsbox,'backgroundcolor',tmp);
			set(grepopts.somewordsbox,'backgroundcolor',tmp);
			done = 0;
		end
		set(grepopts.startsearch,'foregroundcolor','k');
	elseif ~isempty(get(grepopts.usedirbox,'string')) & ~isempty(get(grepopts.extensionsbox,'string'))
		set(grepopts.startsearch,'foregroundcolor','r');
	end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
grepopts = getappdata(gcf, 'grepopts');
% MUSTINCLUDE, MUSTEXCLUDE, SOMEWORDS
mustinclude = get(grepopts.mustincludebox,'string');
% Check for appropriateness of inputs:
if ~ischar(mustinclude)
	set(grepopts.mustincludebox,'backgroundcolor','r');
	error('mustinclude must be a character string.');
end
mustexclude = get(grepopts.mustexcludebox,'string');
if ~ischar(mustexclude)
	set(grepopts.mustexcludebox,'backgroundcolor','r');
	error('mustexclude must be a character string');
end
somewords = get(grepopts.somewordsbox,'string');
if ~ischar(somewords)
	set(grepopts.somewordsbox,'backgroundcolor','r');
	error('somewords must be a character string');
end

if ~isempty(findstr(mustinclude,'"'))
	tmp = mustinclude;
	tmp2 = findstr(mustinclude,'"');
	mustinclude = '';
	if floor(length(tmp2)/2) ~= length(tmp2)/2
		set(grepopts.mustexcludebox,'backgroundcolor','r');
		error('Mismatched quotes in search phrase box.');
	end
	for ii = 1:2:length(tmp2)
		mustinclude = strvcat(mustinclude,tmp(tmp2(ii)+1:tmp2(ii+1)-1));
		drawnow;
	end
end

if ~isempty(get(grepopts.allwordsbox,'string'));
	allwords = get(grepopts.allwordsbox,'string');
	if ~isempty(findstr(allwords,' '))
		tmp = [0 findstr(allwords,' ') size(allwords,2)+1];
		for ii = 1:length(tmp)-1
			mustinclude = strvcat(mustinclude,allwords(tmp(ii)+1:tmp(ii+1)-1));
			drawnow;
		end
	else
		mustinclude = strvcat(mustinclude,allwords);
	end
end
if ~isempty(mustexclude)
	mustexclude = '';
	tmp = get(grepopts.mustexcludebox,'string');
	if ~isempty(findstr(tmp,' '))
		tmp2 = [0 findstr(tmp,' ') size(tmp,2)+1];
		for ii = 1:length(tmp2)-1
			mustexclude = strvcat(mustexclude,tmp(tmp2(ii)+1:tmp2(ii+1)-1));
			drawnow;
		end
	else
		mustexclude = strvcat(mustexclude,tmp);
	end
end
if ~isempty(somewords)
	somewords = '';
	tmp = get(grepopts.somewordsbox,'string');
	if ~isempty(findstr(tmp,' '))
		tmp2 = [0 findstr(tmp,' ') size(tmp,2)+1];
		for ii = 1:length(tmp2)-1
			somewords = strvcat(somewords,tmp(tmp2(ii)+1:tmp2(ii+1)-1));
			drawnow;
		end
	else
		somewords = strvcat(somewords,tmp);
	end
end
exclusionsfcn;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%START SEARCHES
set(grepopts.status,'string','Searching.....');

wholewords = get(grepopts.wholewords,'value');
casesens = get(grepopts.casesensitive,'value');
excludecommented = get(grepopts.excludecomments,'value');
commentsonly = get(grepopts.commentsonly,'value');
commentchar = get(grepopts.commentchar,'string');

currpath = cd;
cd(grepopts.startpath);

%CREATE PADDED STRING OF ALL DIRECTORIES TO SEARCH:
directories = createdirectorieslist;
filenames = {};
set(grepopts.status,'string','Creating directory list and locating filenames...');

%IF .* IS IN EXTENSIONS LIST, "SEARCH ALL" SUPERCEDES ALL OTHER NON-EXCLUSION SPECIFICATIONS
extensions = unique(get(grepopts.extensionsbox,'string'),'rows');
if ismember(extensions,'.*','rows')
	extensions = '.*';
end

% Create a cell list of all QUALIFIED file names on search path; these are the filenames WHICH ARE TO BE SEARCHED
% QUALIFICATION includes: 
%     directory exclusions, date restrictions, size restrictions, and include and exclude extension specs
for ii = 1:size(directories,1)
	try
		[filenames,datelist,sizelist,extensionlist] = ...
			search4files(filenames,datelist,sizelist,extensionlist,deblank(directories(ii,:)),extensions);
	catch
		return;
	end
	drawnow;
end
tmpline = 0;
filedirs = {};
grepopts.filedirs = filedirs;
if ~ casesens
	mustinclude = lower(mustinclude);
	mustexclude = lower(mustexclude);
	somewords = lower(somewords);
end

% Loop through all filenames on search path matching parameters (extensions, dates, sizes, etc.) list
% AT THIS POINT...
%   filenames is an n x 1 cell array of strings, with each element containing a character string path to a file to be analyzed
%   filedirs is an empty cell array

% PROCEDURE: 
% 1) open file
% 2) read in entire contents as a single n x m string
% 3) rule out filenames based on inclusion, exclusion of search strings
% 4) recompile list of filenames matching criteria
% 5) loop through filenames to exclude commented/non-commented/wholeword mismatches
% 6) close file
valid = ones(size(filenames,1),1); %ASSUME FILE IS VALID UNLESS OTHERWISE EXCLUDED
goodchars = char([9,10,13,32:126,161:255]);
skipped=0;
for ii = 1 : size(filenames, 1)
	drawnow;
	if done == 2 %"CANCEL BUTTON" PRESSED
		done = 4; 
		break
	end
	keyword = [];
	filename = filenames{ii};
	set(grepopts.status,'string',sprintf('Searching file %s\n(File %i of %i).....',filename,ii, size(filenames,1))); 
	% Open file(ii) and read contents; determine if file matches selection criteria;
	% write lines from matching filenames to filelines and filedirs
	if ~get(grepopts.filenamesonly,'value')
		tmp = dir(filename);
		fid3 = fopen(filename, 'r');
		if fid3==-1
			skipped=skipped+1
			continue;
		end
		tmpline = [char(10),fread(fid3,tmp.bytes,'*char')',char(10)];
		if get(grepopts.smartsearch,'value')
			tmpline = tmpline(ismember(tmpline,goodchars));
		end
		if ~ casesens
			tmpline = lower(tmpline);
		end
		if any([excludecommented,commentsonly])
			if commentsonly & isempty(findstr(tmpline,commentchar))
				tmpline = '';
			else
				tmpline = reconstruct(tmpline,excludecommented,commentchar); %Since arguments are mutually exclusive, if ~excludecommented then commentsonly
			end
		end
	else
		[pathstr,name,ext] = fileparts(filename);
		tmpline = name;
		if ~ casesens
			tmpline = lower(tmpline);
		end
	end
	for jj = 1:size(mustinclude,1)
		drawnow;
		if wholewords
			wholefound = iswhole(alphachars, deblank(mustinclude(jj,:)), tmpline);
			if ~wholefound
				valid(ii) = 0;
				break
			else
				if isempty(keyword),keyword = deblank(mustinclude(jj,:));end
			end
		else
			% if isempty(findstr(tmpline,deblank(mustinclude(jj,:))))
			if isempty(strfind(tmpline,deblank(mustinclude(jj,:))))
				%this replaced findstr because findstr returned true for,
				%e.g., filename 'efg' mustinclude 'abcdefg'
				valid(ii) = 0;
				break
			else
				if isempty(keyword),keyword = deblank(mustinclude(jj,:));end
			end
		end % if wholewords
		drawnow;
	end 
	if valid(ii) == 1
		for jj = 1:size(mustexclude,1)
			drawnow;
			if wholewords
				wholefound = iswhole(alphachars, deblank(mustexclude(jj,:)), tmpline);
				if wholefound
					valid(ii) = 0;
					break
				end
			else
				if ~isempty(findstr(tmpline,deblank(mustexclude(jj,:))))
					valid(ii) = 0;
					break
				end
			end
			drawnow;
		end
	end
	if valid(ii) == 1 %So far, so good ...
		if ~isempty(somewords)
			valid(ii) = 0;
			for jj = 1:size(somewords,1) %File must contain at least one of these words
				drawnow;
				if wholewords
					wholefound = iswhole(alphachars, deblank(somewords(jj,:)), tmpline);
					if wholefound
						valid(ii) = 1;
						if isempty(keyword),keyword = deblank(somewords(jj,:));end
						break                        
					end
				else
					if ~isempty(findstr(tmpline,deblank(somewords(jj,:))))
						valid(ii) = 1;
						if isempty(keyword),keyword = deblank(somewords(jj,:));end
						break
					end
				end
			end
		end
	end
	%Reconstruct line showing keyword
	if valid(ii) == 1 %Get line  information
		if ~get(grepopts.filenamesonly,'value') 
			posn = findstr(keyword,tmpline);
			posn = posn(1);    
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			%Find previous and subsequent line break
			hardreturns = find(tmpline == 10 | tmpline == 13);
			tmpstart = max(hardreturns(hardreturns < posn));
			tmpend = min(hardreturns(hardreturns > posn));
			tmpline = tmpline(tmpstart+1:tmpend-1);
			%If we got to this point, the file is valid.
		end
		filelines{size(filelines, 1) + 1, 1} = tmpline;
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	end
	if ~get(grepopts.filenamesonly,'value')
		% Close file(ii) and continue
		fclose(fid3);
	end
	drawnow;
end % for ii = 1 : size(filenames, 1)

if done ~=4
	filedirs = filenames(logical(valid),:);
	grepopts.filedirs = filedirs;
	datelist = datelist(logical(valid),:);
	sizelist = sizelist(logical(valid),:);
	extensionlist = extensionlist(logical(valid),:);
	
	if sum(cellfun('length',grepopts.filedirs)~=1) == 0 %size(grepopts.filedirs,1) == 0
		set(grepopts.status,'string','Search complete. No filenames found matching selected criteria.');
	elseif sum(cellfun('length',grepopts.filedirs)~=1) == 1 %size(grepopts.filedirs,1) == 1
		set(grepopts.status,'string',{sprintf('Search complete. One file found matching selected criteria.'),...
				'(Select file to activate EDIT button.)'} );
	else
		set(grepopts.status,'string',{sprintf('Search complete. %i filenames found matching selected criteria.',sum(cellfun('length',grepopts.filedirs)~=1)),...%size(grepopts.filedirs,1)
				'(Select file(s) to activate EDIT button.)'} );
	end
	cd(currpath);
	set(gcf,'name','FileGrep Results');
	%set(h_stext1,'string','------------------- RESULTS: -------------------','fontweight','b','fontsize',14);
	set(h_stext1,'visible','off');
	%set(findobj('tag','resultsstring'),'visible','on');
	set(findobj('tag','frame1'),'visible','off');
	set(findobj('tag','frame2'),'visible','off');
	%grepopts.numitems = size(grepopts.filedirs,1);
	grepopts.numitems = sum(cellfun('length',grepopts.filedirs)~=1);
	filedirs{end+1}=' ';
	grepopts.filedirs = filedirs;
	%     filedirs{end+1}=' ';
	set(grepopts.selectallbutton,'visible','on');
% 	set(grepopts.selectalltext,'visible','on');
	set(grepopts.resultsbox,'visible','on','value',[],'string',grepopts.filedirs);
	set(grepopts.sortresultsbutton,'visible','on');
	set(grepopts.sortstringbutton,'visible','on');
	filelines{end+1}=' ';
	set(grepopts.stringbox,'visible','on','value',[],'string',filelines);
	datelist{end+1}=' ';
	%     datelist{end+1}=' ';
	set(grepopts.datebox,'visible','on','value',[],'string',datelist);
	set(grepopts.sortdatebutton,'visible','on');
	extensionlist{end+1}=' ';
	%     extensionlist{end+1}=' ';
	set(grepopts.extensionbox,'visible','on','value',[],'string',extensionlist);
	set(grepopts.sortextensionbutton,'visible','on');
	sizelist{end+1}=' ';
	%     sizelist{end+1}=' ';
	set(grepopts.sizebox,'visible','on','value',[],'string',sizelist);
	set(grepopts.sortsizebutton,'visible','on');
	set(grepopts.editselectedbutton,'visible','on');
	set(grepopts.hideselectedbutton,'visible','on');
	set(grepopts.keepselectedbutton,'visible','on');
	set(grepopts.deleteselectedbutton,'visible','on');
	set(grepopts.replacestringbutton,'visible','on');
	set(grepopts.savefilebutton,'visible','on');
	set(grepopts.scrollresults,'visible','on','max',max(1,grepopts.numitems-30),...
		'value',max(1,grepopts.numitems-30),'sliderstep',[1/(grepopts.numitems+1) 30/(grepopts.numitems+1)]);
	showparameters;
	filedirs(end) = [];
	grepopts.filedirs = filedirs;
	filelines(end) = [];
	set(cancelit,'string','Quit','callback','delete(gcf)');
	set(grepopts.startsearch,'string','New Search','callback',...
		'global done; done = 0; grepopts = getappdata(gcf,''grepopts''); [grepopts.filedirs, filelines] = filegrep(grepopts);');
	setappdata(gcf,'grepopts',grepopts);
	done = 1;
	boxinds = [grepopts.resultsbox,grepopts.stringbox,grepopts.datebox,grepopts.extensionbox,grepopts.sizebox];
	while ~done
		drawnow;%%%%
		try
			tmp1 = cell2mat(get(boxinds,'listboxtop'));
			if length(unique(tmp1))~=1
				setdiff(tmp1,mode(tmp1))
				set(boxinds,'listboxtop',setdiff(tmp1,mode(tmp1)));
			end
			drawnow; %Very important here...program crashes without this statement!
		catch
			done = 1;
		end %try
	end %while ~done
end %if done ~=4


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SUBFUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function adddirectoryfcn
grepopts = getappdata(gcf,'grepopts');
if ~isfield(grepopts,'exclusions')
	exclusionsfcn;
end
tmp = get(grepopts.extensionsbox,'string');
% THIS WORKS: [filename, pathname] = uigetfile({'*.txt; *.mdl; *.m; *.*'},'Pick a file')

if grepopts.rel < 13
	if get(grepopts.monly,'value') == 1
		[grepopts.filename,grepopts.startpath] = uigetfile([cd filesep '*.m'],'Select any m-file in the desired starting path');
	elseif get(grepopts.txtonly,'value') == 1
 		[grepopts.filename,grepopts.startpath] = uigetfile([cd filesep '*.txt'],'Select any txt-file in the desired starting path');
	else
		[grepopts.filename,grepopts.startpath] = uigetfile([cd filesep '*.*'],'Select any file in the desired starting path');
	end
else
	grepopts.filename = '';
	grepopts.startpath = uigetdir(cd,'Add directory to search path');
end
if grepopts.startpath == 0
	return
end
currusedirboxstring = get(grepopts.usedirbox,'string');
currdirs = currusedirboxstring(:,3:end); %Characters 1 and 2 are reserved for +/- (and space), indicating recursion value
if ~ismember(grepopts.startpath, currdirs ,'rows')
	recursecurrent = questdlg('INCLUDE:','Current Selection Options','Selected Directory + Subfolders',...
		'Selected Directory','Cancel','Selected Directory + Subfolders');
	if strcmp(recursecurrent,'Selected Directory + Subfolders')
		set(grepopts.usedirbox,'string',strvcat(get(grepopts.usedirbox,'string'), ['+ ', grepopts.startpath]),'value',[]);
		recursecurrent = 1;
	elseif strcmp(recursecurrent,'Selected Directory')
		set(grepopts.usedirbox,'string',strvcat(get(grepopts.usedirbox,'string'), ['- ', grepopts.startpath]),'value',[]);
		recursecurrent = 0;
	end
else
	inclnonrecurs = find(ismember(currusedirboxstring,['- ', grepopts.startpath], 'rows'));
	if ~isempty(inclnonrecurs) %path was previously added non-recursively
		addsubdirs = questdlg('Current selection (without subdirectories) is already included included in search path. Do you want to add its subdirectories?','Ensure subdirectory inclusion?','Add Subdirectories','Cancel','Add Subdirectories');
		if strcmp(addsubdirs,'Add Subdirectories')
			recursecurrent = 1;
			currusedirboxstring(inclnonrecurs,:) = [];
			set(grepopts.usedirbox,'string',strvcat(currusedirboxstring, ['+ ', grepopts.startpath]),'value',[]);
		end
	else %path was previously added recursively
		set(grepopts.status,'string','Selected directory and its subfolders are already in search list.');
		pause(2);
		set(grepopts.status,'string','');
		recursecurrent = 'Cancel';
	end
end
if strcmp(recursecurrent,'Cancel')
	return
end
setappdata(gcf,'grepopts',grepopts);
if get(grepopts.prescreenextensions,'value') %Prescreen option is selected 
	if ~get(grepopts.monly,'value') & ~get(grepopts.txtonly,'value')
		updateextensions(grepopts.startpath, recursecurrent);
		set(grepopts.extensionsbox,'value',[]);
	elseif get(grepopts.monly,'value')
		grepopts.extensions = '.m';
	else %monly, txtonly are mutually exclusive; at this point, txtonly must be true
		grepopts.extensions = '.txt';
	end
end
set(grepopts.status,'string','');
setappdata(gcf,'grepopts',grepopts);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function removedirectoryfcn
grepopts = getappdata(gcf,'grepopts');
tmpstr = get(grepopts.usedirbox,'string');
vals = get(grepopts.usedirbox,'value');
tmpstr(vals,:)=[];
set(grepopts.usedirbox,'value',[],'string',tmpstr);
setappdata(gcf,'grepopts',grepopts);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function updateextensions(startpath,recurse)
grepopts = getappdata(gcf,'grepopts');
if recurse
	%Create string of recursive directories/subdirectories
	set(grepopts.status,'string','Generating path definitions...');
	paths = genpath(startpath);
	%Find instances of the path separator
	seplocs = findstr(paths,pathsep);
	%Parse paths into individual (vertically catenated) directories
	if ~isempty(seplocs)
		directories = paths(1:seplocs(1)-1);
		for ii = 1:length(seplocs)-1
			drawnow;
			directories = strvcat(directories,paths(seplocs(ii)+1:seplocs(ii+1)-1));
		end
	end
else
	directories = startpath;
end
set(grepopts.status,'string','Creating extensions list...');
for ii = 1:size(directories,1)
	tmp = dir([deblank(directories(ii,:)) filesep '*.*']);
	if ~isempty(tmp),tmp = char(tmp.name);end
	%Update filenames to reflect newly detected filenames. (Omit trailing blanks.)
	for jj = 1:size(tmp,1)
		currextensions = get(grepopts.extensionsbox,'string');
 		tmpfile = [deblank(directories(ii,:)) filesep deblank(tmp(jj,:))];
		[pathstr,name,ext] = fileparts(tmpfile);
		ext = lower(ext);
		if ~strcmp(ext,'.') & ~strcmp(ext,'..') & ~isempty(ext)
			tmpext = [ext repmat(' ',1,size(grepopts.exclusions,2)-size(ext,2))];
			if ~ismember(ext,grepopts.exclusions,'rows')
				set(grepopts.extensionsbox,'string',unique(strvcat(lower(get(grepopts.extensionsbox,'string')),ext),'rows'));
			end
		end
		drawnow;
	end
	drawnow;
end
setappdata(gcf,'grepopts',grepopts);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function directories = createdirectorieslist
grepopts = getappdata(gcf,'grepopts');
set(grepopts.status,'string','Creating directory(ies) list....');
ckdirs = get(grepopts.usedirbox,'string');
directories = '';
for ii = 1:size(ckdirs,1)
	tmpdir = deblank(ckdirs(ii,:));
	recurse = strcmp(tmpdir(1),'+');
	tmpdir = tmpdir(3:end);
	if recurse
		set(grepopts.status,'string','Generating path definitions...');
		paths = genpath(tmpdir);
		%Find instances of the path separator
		seplocs = findstr(paths,pathsep);
		%Parse paths into individual (vertically catenated) directories
		if ~isempty(seplocs)
			directories = strvcat(directories,paths(1:seplocs(1)-1));
			for jj = 1:length(seplocs)-1
				directories = strvcat(directories,paths(seplocs(jj)+1:seplocs(jj+1)-1));
			end
		end
	else
		directories = strvcat(directories,tmpdir);
	end
	drawnow;
end
set(grepopts.status,'string','');
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [filenames,datelist,sizelist,extensionlist] = search4files(filenames,datelist,sizelist,extensionlist,path,extensions)
% Create a cell list of all QUALIFIED file names on search path
% QUALIFICATION includes: 
%     directory exclusions, date restrictions, size restrictions, and include and exclude extension specs
% Example:
% filenames = 
%     'C:\Brett\MFiles\\GLCM.m'
%     'C:\Brett\MFiles\\GLCMbu.m'
%     'C:\Brett\MFiles\\adjustit.m'
%     'C:\Brett\MFiles\\affixright.m'

grepopts = getappdata(gcf,'grepopts');
set(grepopts.status,'string',sprintf('Searching path %s for qualified filenames.',path));
%Generate an all-filenames directory of the current path 
tmpdir = dir([path filesep '*.*']);
if ~isempty(tmpdir)
	valid = ones(size(tmpdir,1),1);
	%Determine parameters for date restrictions
	if ~get(grepopts.ignoredates,'value')
		if get(grepopts.modday,'value') %Today only
			minvaldate = floor(datenum(now));
			maxvaldate = minvaldate;
		elseif get(grepopts.modmonth,'value') %This month only
			minvaldate = floor(datenum(now))-30;
			maxvaldate = floor(datenum(now));
		elseif get(grepopts.modyear,'value') %This year only
			minvaldate = floor(datenum(now))-365;
			maxvaldate = floor(datenum(now));
		elseif get(grepopts.datelimit,'value') %Specify dates
			minvaldate = floor(datenum(get(grepopts.datemin,'string')));
			maxvaldate = floor(datenum(get(grepopts.datemax,'string')));
		else
			error('Improper assignment to grepopts.ignoredates.');
		end
	end
	%Determine parameters for size restrictions
	if ~get(grepopts.ignoresizes,'value')
		if get(grepopts.smallfiles,'value') %Small files only
			minvalsize = 0;
			maxvalsize = 1e5;
		elseif get(grepopts.mediumfiles,'value') %Small and medium files only
			minvalsize = 0;
			maxvalsize = 1e6;
		elseif get(grepopts.largefiles,'value') %Large files only
			minvalsize = 1e6;
			maxvalsize = inf;
		elseif get(grepopts.sizelimit,'value') %Specify sizes
			if get(grepopts.usekb,'value')
				sizemult = 1e3;
			elseif get(grepopts.usemb,'value')
				sizemult  = 1e6;
			else
				error('Improper assigment to sizemult.');
			end
			minvalsize = str2num(get(grepopts.sizemin,'string'))*sizemult;
			maxvalsize = str2num(get(grepopts.sizemax,'string'))*sizemult;
		else
			error('Improper assignment to grepopts.ignoresizes.');
		end
	end
end
%Qualify the directory entries item by item
for ff = 1:length(valid)
	for kk = 1:1 %use a for-loop structure to enable break when valid==0 is determined
		%EXCLUDE DIRECTORIES
		if tmpdir(ff).isdir
			valid(ff) = 0;
			break
		end
		%EXCLUDE FOR DATE RESTRICTIONS
		if ~get(grepopts.ignoredates,'value')
			if floor(datenum(tmpdir(ff).date)) < minvaldate | floor(datenum(tmpdir(ff).date)) > maxvaldate
				valid(ff) = 0;
				break
			end
		end
		%EXCLUDE FOR SIZE RESTRICTIONS
		if ~get(grepopts.ignoresizes,'value')
			if tmpdir(ff).bytes < minvalsize | tmpdir(ff).bytes > maxvalsize
				valid(ff) = 0;
				break
			end
		end
		%EXCLUDE FOR EXTENSION RESTRICTIONS
		[pathstr,name,ext] = fileparts(tmpdir(ff).name);
		if ismember(ext,grepopts.exclusions,'rows')
			valid(ff) = 0;
			break
		end
		drawnow;
	end
	drawnow;
end
tmpdir = tmpdir(logical(valid),:);
%Update filenames to reflect newly detected filenames. (Omit trailing blanks.)

for ii = 1:size(tmpdir,1)
	[pathstr,tmpfile,ext] = fileparts(tmpdir(ii).name);
	if ismember(ext,extensions,'rows') | strcmp(extensions, '.*')
		filenames{size(filenames,1)+1,1} = fullfile(path,tmpdir(ii).name);
		datelist{size(datelist,1)+1,1} = tmpdir(ii).date;
		sizelist{size(sizelist,1)+1,1} = tmpdir(ii).bytes/1000;
		extensionlist{size(extensionlist,1)+1,1} = ext;
	end
	drawnow;
end
set(grepopts.status,'string','');
setappdata(gcf,'grepopts',grepopts);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function wholefound = iswhole(alphachars, searchforstr, teststring)
prevmbrs = 1; postmbrs = 1; delprevposns = []; delpostposns = [];
charlocs = findstr(searchforstr, teststring);
if ~isempty(charlocs)
	prevlocs = charlocs - 1;
	postlocs = charlocs + length(searchforstr);
	delprevposns = [find(prevlocs == 0)];
	delpostposns = [find(postlocs >= length(teststring))];
	prevlocs(delprevposns) = '';
	postlocs(delpostposns) = '';
	prevmbrs = ismember(teststring(prevlocs)', alphachars);
	postmbrs = ismember(teststring(postlocs)', alphachars);
end
if ~ isempty(delprevposns), prevmbrs = [0; prevmbrs]; end
if ~ isempty(delpostposns), postmbrs = [postmbrs; 0]; end
wholefound = any(~ or(prevmbrs, postmbrs));
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function moveleftfcn
grepopts=getappdata(gcf,'grepopts');
currstring = get(grepopts.extensionsbox,'string');
if ~isempty(currstring)
	vals = get(grepopts.extensionsbox,'value');
	tmp = unique(strvcat(currstring(vals,:),get(grepopts.excludebox,'string')),'rows');
	tmp = setdiff(tmp,'.*','rows');
	if ~isstr(tmp),tmp = ''; end
	set(grepopts.excludebox,'value',[],'string',tmp);
	currstring(vals,:)=[];
	set(grepopts.extensionsbox,'value',[],'string',currstring);
end
set(grepopts.monly,'value',0);
set(grepopts.txtonly,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function moverightfcn
grepopts=getappdata(gcf,'grepopts');
currstring = get(grepopts.excludebox,'string');
if ~isempty(currstring)
	set(grepopts.monly,'value',0);
	set(grepopts.txtonly,'value',0);
	vals = get(grepopts.excludebox,'value');
	tmp = unique(strvcat(currstring(vals,:),get(grepopts.extensionsbox,'string')),'rows');
	set(grepopts.extensionsbox,'value',[],'string',tmp);
	currstring(vals,:)=[];
	set(grepopts.excludebox,'value',[],'string',currstring);
end
set(grepopts.monly,'value',0);
set(grepopts.txtonly,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function moveextleftfcn
grepopts = getappdata(gcf,'grepopts');
tmp = get(grepopts.sel_ext,'string');
if ~isempty(tmp)
	if ~strcmp(tmp(1),'.')
		tmp = ['.' tmp];
	end
	if  ~strcmp(tmp,'.*')
		set(grepopts.excludebox,'string',unique(strvcat(get(grepopts.excludebox,'string'),tmp),'rows'),'value',[]);
	end
	set(grepopts.sel_ext,'string','');
end
set(grepopts.monly,'value',0);
set(grepopts.txtonly,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function moveextrightfcn
grepopts = getappdata(gcf,'grepopts');
tmp = get(grepopts.sel_ext,'string');
if ~isempty(tmp)
	if ~strcmp(tmp(1),'.')
		tmp = ['.' tmp];
	end
	set(grepopts.extensionsbox,'string',unique(strvcat(get(grepopts.extensionsbox,'string'),tmp),'rows'),'value',[]);
	set(grepopts.sel_ext,'string','');
end
set(grepopts.monly,'value',0);
set(grepopts.txtonly,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function selallleftfcn
grepopts=getappdata(gcf,'grepopts');
currstring = get(grepopts.excludebox,'string');
set(grepopts.excludebox,'value',[],'string','');
tmp = unique(strvcat(currstring,get(grepopts.extensionsbox,'string')),'rows');
set(grepopts.extensionsbox,'value',[],'string',tmp);
set(grepopts.monly,'value',0);
set(grepopts.txtonly,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function selallrightfcn
grepopts=getappdata(gcf,'grepopts');
currstring = get(grepopts.extensionsbox,'string');
currstring = setdiff(currstring,'.*','rows');
if ~isstr(currstring),currstring = ''; end
set(grepopts.extensionsbox,'value',[],'string','');
tmp = unique(strvcat(currstring,get(grepopts.excludebox,'string')),'rows');
set(grepopts.excludebox,'value',[],'string',tmp);
set(grepopts.monly,'value',0);
set(grepopts.txtonly,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function monlyfcn
grepopts=getappdata(gcf,'grepopts');

if get(grepopts.monly,'value') == 1
	set(grepopts.txtonly,'value',0);
	set(grepopts.excludebox,'value',[],'string','');
	set(grepopts.extensionsbox,'value',[],'string','.m')
end
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function txtonlyfcn
grepopts=getappdata(gcf,'grepopts');

if get(grepopts.txtonly,'value') == 1
	set(grepopts.monly,'value',0);
	set(grepopts.excludebox,'value',[],'string','');
	set(grepopts.extensionsbox,'value',[],'string','.txt');
end
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function editselectedfcn
global done
done = 1;
grepopts = getappdata(gcf,'grepopts');
filenames = get(grepopts.resultsbox,'string');
vals = get(grepopts.resultsbox,'value');

for ii = 1:length(vals)
	[path,name,ext] = fileparts(filenames{vals(ii)});
	if strcmp(filenames{vals(ii)},char(32))
		continue;
	end		
	ext = lower(ext);
	if ~ismember(ext,{'.fig','.exe','.nb','.mat','.m'})
		try
			winopen(filenames{vals(ii)});
			continue;
		end
	end
	switch ext
		case {'.dat','.dll','.drv','.frk','.ini','.sys'}
			beep;
			set(grepopts.status,'string',sprintf('Sorry, this program does not provide editing/opening capabilities for files of type ''%s''.\n',ext));
			continue;
		case '.fig'
			openfig(filenames{vals(ii)},'new','visible');
		case {'.exe','.nb'}
			system(['start ',filenames{vals(ii)}]);
		case '.m'
			edit(filenames{vals(ii)});
		case '.mat'
			load(filenames{vals(ii)});
			set(grepopts.status,'string',sprintf('File %s loaded into MATLAB''s workspace.',filenames{vals(ii)}));
		case {'.gif','.hdf','.ico','.pcx','.png','.xwd'}
			try
				dos(['start mspaint "', filenames{vals(ii)},'"']);
				continue;
			end
		otherwise
			openlist = openwith(ext);
			if ~isempty(openlist)
				for jj = 1:size(openlist,1)
					[tmp1,tmp2,tmp3]=fileparts(openlist(jj,:));
					tmp2=lower(tmp2);
					if strcmp(tmp2,'matlab')
						try
							edit(filenames{vals(ii)});
							continue
						end
					else
						try
							system(['start ', tmp2, ' "',filenames{vals(ii)},'"']);
							continue;
						end
						try
							edit(filenames{vals(ii)});
							continue
						end
						try
							selectprogramfcn(filenames{vals(ii)},ext);
							continue
						end
						beep;
						set(grepopts.status,'string',sprintf('Sorry, can''t seem to open/edit file %s.',filenames{vals(ii)}));
					end
				end
			end
	end
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function selectprogramfcn(tmp,ext)
clear fname; global fname; fname = tmp;
close(findobj('name','Select Program'));
figpos = centerfig(40,40);
progselectfig = figure('name','Select Program','numbertitle','off',...
	'menubar','none','position',figpos,'resize','off','color',[0.46 0.72 0.73]);
uicontrol(progselectfig,'style','text','units','normalized','position',[0.1 0.8 0.8 0.15],...
	'string',sprintf('Unable to open file %s.',fname),...
	'fontsize',10,'backgroundcolor',[0.46 0.72 0.73],...
	'horizontalalignment','center','fontweight','b');
uicontrol(progselectfig,'style','text','units','normalized','position',[0.1 0.55 0.8 0.15],...
	'string',sprintf('Skip file\n%s',fname),...
	'fontsize',10,'backgroundcolor',[0.46 0.72 0.73],...
	'horizontalalignment','center','fontweight','b');
uicontrol(progselectfig,'style','pushbutton','units','normalized','position',[0.1 0.45 0.8 0.1],...
	'string','Skip','fontsize',12,'fontweight','b','callback','delete(gcf);');
uicontrol(progselectfig,'style','text','units','normalized','position',[0.1 0.1 0.8 0.15],...
	'string','Or BROWSE to Select Program...',...
	'fontsize',10,'backgroundcolor',[0.46 0.72 0.73],...
	'horizontalalignment','center','fontweight','b');
uicontrol(progselectfig,'style','pushbutton','units','normalized','position',[0.1 0.05 0.8 0.1],...
	'string','...','fontsize',18,'fontweight','b','callback',...
	['global fname;',...
		'[tmp1,tmp2]=uigetfile(''*.exe'');',...
		'[tmp1,tmp2] = fileparts(tmp1);',...
		'try;',...
		'system([''start '', tmp2,'' "'', fname,''"'']);',...
		'delete(gcf);',...
		'catch;',...
		'h=msgbox(sprintf(''Unable to open file %s.'',fname));',...
		'uiwait(h);',...
		'end;']);
clear fname;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function hideselectedfcn
grepopts = getappdata(gcf,'grepopts');
filenames = get(grepopts.resultsbox,'string');
dates = get(grepopts.datebox,'string');
strings = get(grepopts.stringbox,'string');
extensions = get(grepopts.extensionbox,'string');
sizes = get(grepopts.sizebox,'string');
vals = get(grepopts.resultsbox,'value');
set(grepopts.resultsbox,'value',[],'string',filenames(setdiff(1:size(filenames,1),vals),:));
set(grepopts.stringbox,'value',[],'string',strings(setdiff(1:size(filenames,1),vals),:));
set(grepopts.datebox,'value',[],'string',dates(setdiff(1:size(filenames,1),vals),:));
set(grepopts.extensionbox,'value',[],'string',extensions(setdiff(1:size(filenames,1),vals),:));
set(grepopts.sizebox,'value',[],'string',sizes(setdiff(1:size(filenames,1),vals),:));
setappdata(gcf,'grepopts',grepopts);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function keepselectedfcn
grepopts = getappdata(gcf,'grepopts');
filenames = get(grepopts.resultsbox,'string');
dates = get(grepopts.datebox,'string');
strings = get(grepopts.stringbox,'string');
extensions = get(grepopts.extensionbox,'string');
sizes = get(grepopts.sizebox,'string');
vals = get(grepopts.resultsbox,'value');
set(grepopts.resultsbox,'value',[],'string',filenames(vals,:));
set(grepopts.stringbox,'value',[],'string',strings(vals,:));
set(grepopts.datebox,'value',[],'string',dates(vals,:));
set(grepopts.extensionbox,'value',[],'string',extensions(vals,:));
set(grepopts.sizebox,'value',[],'string',sizes(vals,:));
setappdata(gcf,'grepopts',grepopts);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function deleteselectedfcn
really = questdlg('Do you want to move files to a temporary folder, or permanently delete the files?','Move or Delete Files','MOVE THEM','Delete Them','Cancel','MOVE THEM');
if strcmp(really,'Delete Them')
	h=warndlg('PROCEED WITH CAUTION...THIS WILL PERMANENTLY DELETE YOUR FILES!');
	uiwait(h);
elseif strcmp(really,'MOVE THEM')
	created = 0;
	h=0;
elseif strcmp(really,'Cancel')
	return;
end
grepopts = getappdata(gcf,'grepopts');
filenames = get(grepopts.resultsbox,'string');
vals = get(grepopts.resultsbox,'value');
for ii = 1:length(vals)
	drawnow;
	if strcmp(really,'Delete Them')
		verified = questdlg(sprintf('Are you sure you want to delete file %s?',filenames{vals(ii)}),'Confirm Deletion','YES','Skip','Cancel','YES');
	elseif strcmp(really,'MOVE THEM')
		verified = 'YES';
	end
	if strcmp(verified,'Cancel')
		break
	end
	if strcmp(verified,'YES')
		if strcmp(really,'MOVE THEM') & ~created
			tmp=mkdir(cd,'MOVED_FILES');
			if ~tmp
				error('Sorry, there is problem creating folder ''MOVED_FILES'' in the current directory.');
			elseif ~h
				h=msgbox(sprintf('File(s) will be moved to folder %s.',[cd filesep 'MOVED_FILES']));
				uiwait(h);
			end
		end
		tmpvals = get(grepopts.resultsbox,'value');
		newfilenames = get(grepopts.resultsbox,'string');
		highlightedfiles = newfilenames(tmpvals,:);
		newfilenames=newfilenames(~ismember(newfilenames,filenames{vals(ii)}),:);
		set(grepopts.resultsbox,'string',newfilenames);
		[a,newvals]=intersect(newfilenames,highlightedfiles);
		newdates = get(grepopts.datebox,'string');
		newstrings = get(grepopts.stringbox,'string');
		newextensions = get(grepopts.extensionbox,'string');
		newsizes = get(grepopts.sizebox,'string');
		if strcmp(really,'Delete Them')
			delete(filenames{vals(ii)});
		elseif strcmp(really,'MOVE THEM')
			tmp=movefile(filenames{vals(ii)},[cd filesep 'MOVED_FILES']);
			if ~tmp,
				error(sprintf('Problem encountered moving file %s. Remaining attempt(s) aborted.',filenames{vals(ii)}));
			end
		end
		set(grepopts.resultsbox,'value',newvals);
		set(grepopts.stringbox,'value',newvals,'string',newstrings(setdiff(1:size(newfilenames,1),vals(ii)),:));
		set(grepopts.datebox,'value',newvals,'string',newdates(setdiff(1:size(newfilenames,1),vals(ii)),:));
		set(grepopts.extensionbox,'value',newvals,'string',newextensions(setdiff(1:size(newfilenames,1),vals(ii)),:));
		set(grepopts.sizebox,'value',newvals,'string',newsizes(setdiff(1:size(newfilenames,1),vals(ii)),:));
	end
end %for ii = 1:length(vals)

return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function keepselectedleftfcn
grepopts = getappdata(gcf,'grepopts');
vals = get(grepopts.excludebox,'value');
currstring = get(grepopts.excludebox,'string');
set(grepopts.excludebox,'value',[],'string',currstring(vals,:));
tmp = unique(strvcat(currstring(setdiff(1:size(currstring,1),vals),:),get(grepopts.extensionsbox,'string')),'rows');
if ~isstr(tmp),tmp = ''; end
set(grepopts.extensionsbox,'value',[],'string',tmp);
set(grepopts.monly,'value',0);
set(grepopts.txtonly,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function clearleftfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.excludebox,'value',[],'string','');
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function keepselectedrightfcn
grepopts = getappdata(gcf,'grepopts');
vals = get(grepopts.extensionsbox,'value');
currstring = get(grepopts.extensionsbox,'string');
set(grepopts.extensionsbox,'value',[],'string',currstring(vals,:));
tmp = unique(strvcat(currstring(setdiff(1:size(currstring,1),vals),:),get(grepopts.excludebox,'string')),'rows');
if ~isstr(tmp),tmp = ''; end
set(grepopts.excludebox,'value',[],'string',tmp);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function clearrightfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.extensionsbox,'value',[],'string','');
set(grepopts.monly,'value',0);
set(grepopts.txtonly,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function commentsonlyfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.excludecomments,'value',0);
if get(grepopts.commentsonly,'value')
	set(grepopts.commentchar,'enable','on');
else
	set(grepopts.commentchar,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function excludecommentsfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.commentsonly,'value',0);
if get(grepopts.excludecomments,'value')
	set(grepopts.commentchar,'enable','on');
else
	set(grepopts.commentchar,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DATE FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ignoredatesfcn
grepopts = getappdata(gcf,'grepopts');
if get(grepopts.ignoredates,'value')
	set(findobj('userdata','dateradio'),'value',0);
else
	set(grepopts.ignoredates,'value',1);
end
if get(grepopts.datelimit,'value')
	set(grepopts.datemin,'enable','on');
	set(grepopts.datemax,'enable','on');
else
	set(grepopts.datemin,'enable','off');
	set(grepopts.datemax,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function usedatesfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.ignoredates,'value',0);
set(findobj('userdata','dateradio'),'value',0);
set(gco,'value',1);
if get(grepopts.datelimit,'value')
	set(grepopts.datemin,'enable','on');
	set(grepopts.datemax,'enable','on');
else
	set(grepopts.datemin,'enable','off');
	set(grepopts.datemax,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function todayonlyfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.datelimit,'value',0);
set(grepopts.datemin,'enable','off');
set(grepopts.datemax,'enable','off');
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function datelimitfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.ignoredates,'value',0);
set(findobj('userdata','dateradio'),'value',0);
set(gco,'value',1);
if get(grepopts.datelimit,'value')
	set(grepopts.datemin,'enable','on');
	set(grepopts.datemax,'enable','on');
else
	set(grepopts.datemin,'enable','off');
	set(grepopts.datemax,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dateminfcn
grepopts = getappdata(gcf,'grepopts');
try
	mindatenum = datenum(get(grepopts.datemin,'string'));
	tmpdate = get(grepopts.datemin,'string');
	set(grepopts.datemin,'string',datestr(mindatenum));
catch
	beep;
	set(grepopts.datemin,'string',datestr(datenum(now)-365,1));
	return
end
maxdatenum = datenum(get(grepopts.datemax,'string'));
if mindatenum > maxdatenum | str2num(tmpdate(1:2)) > 31
	beep;
	set(grepopts.datemin,'string',datestr(datenum(now)-365,1));
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function datemaxfcn
grepopts = getappdata(gcf,'grepopts');
try
	maxdatenum = datenum(get(grepopts.datemax,'string'));
	tmpdate = get(grepopts.datemax,'string');
	set(grepopts.datemax,'string',datestr(maxdatenum));
catch
	beep;
	set(grepopts.datemax,'string',date);
	return
end
mindatenum = datenum(get(grepopts.datemin,'string'));
if mindatenum > maxdatenum | str2num(tmpdate(1:2)) > 31
	beep;
	set(grepopts.datemax,'string',date);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SIZE FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ignoresizesfcn
grepopts = getappdata(gcf,'grepopts');
if get(grepopts.ignoresizes,'value')
	set(findobj('userdata','sizeradio'),'value',0);
else
	set(grepopts.ignoresizes,'value',1);
end
if get(grepopts.sizelimit,'value')
	set(grepopts.sizemin,'enable','on');
	set(grepopts.sizemax,'enable','on');
	set(grepopts.usekb,'enable','on');
	set(grepopts.usemb,'enable','on');
else
	set(grepopts.sizemin,'enable','off');
	set(grepopts.sizemax,'enable','off');
	set(grepopts.usekb,'enable','off');
	set(grepopts.usemb,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function usesizesfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.ignoresizes,'value',0);
set(findobj('userdata','sizeradio'),'value',0);
set(gco,'value',1);
if get(grepopts.sizelimit,'value')
	set(grepopts.sizemin,'enable','on');
	set(grepopts.sizemax,'enable','on');
	set(grepopts.usekb,'enable','on');
	set(grepopts.usemb,'enable','on');
else
	set(grepopts.sizemin,'enable','off');
	set(grepopts.sizemax,'enable','off');
	set(grepopts.usekb,'enable','off');
	set(grepopts.usemb,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function sizelimitfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.ignoresizes,'value',0);
set(findobj('userdata','sizeradio'),'value',0);
set(gco,'value',1);
if get(grepopts.sizelimit,'value')
	set(grepopts.sizemin,'enable','on');
	set(grepopts.sizemax,'enable','on');
	set(grepopts.usekb,'enable','on');
	set(grepopts.usemb,'enable','on');
else
	set(grepopts.sizemin,'enable','off');
	set(grepopts.sizemax,'enable','off');
	set(grepopts.usekb,'enable','off');
	set(grepopts.usemb,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function sizeminfcn
grepopts = getappdata(gcf,'grepopts');
if isempty(str2num(get(grepopts.sizemin,'string'))) | str2num(get(grepopts.sizemin,'string')) < 0
	beep;
	set(grepopts.sizemin,'string',0);
	return
end
if str2num(get(grepopts.sizemin,'string')) > str2num(get(grepopts.sizemax,'string'))
	beep;
	set(grepopts.sizemin,'string',0);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function sizemaxfcn
grepopts = getappdata(gcf,'grepopts');
if isempty(str2num(get(grepopts.sizemax,'string')))
	beep;
	set(grepopts.sizemax,'string',50);
	return
end
if str2num(get(grepopts.sizemin,'string')) > str2num(get(grepopts.sizemax,'string'))
	beep;
	set(grepopts.sizemax,'string',50);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function usekbfcn
grepopts = getappdata(gcf,'grepopts');
if get(grepopts.usekb,'value')
	set(grepopts.usemb,'value',0);
else
	set(grepopts.usemb,'value',1);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function usembfcn
grepopts = getappdata(gcf,'grepopts');
if get(grepopts.usemb,'value')
	set(grepopts.usekb,'value',0);
else
	set(grepopts.usekb,'value',1);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function checkvaluesfcn(varargin)
grepopts = getappdata(gcf,'grepopts');
if nargin == 0
	if get(grepopts.smartsearch,'value') & ~ isempty(intersect(double(get(gcbo,'string')),[1:8,11:12,14:31,127:160]))
		msgstr = sprintf('It appears that you want to search for characters excluded by the\nSmart Search option.\nYou might consider de-selecting the "Smart Search" checkbox.');
		tmp=warndlg(msgstr,'Disable SmartSearch!');
		uiwait(tmp);
	end
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function addext(ext)
grepopts = getappdata(gcf,'grepopts');
currext = get(grepopts.extensionsbox,'string');
if strcmp(ext,'.*') | strcmp(get(grepopts.extensionsbox,'string'),'.*')
	set(grepopts.extensionsbox,'value',[],'string','.*');
	set(grepopts.txtonly,'value',0);
	set(grepopts.monly,'value',0);
else
	set(grepopts.extensionsbox,'string',unique(strvcat(currext,ext),'rows'),'value',[]);
	tmp = setdiff(get(grepopts.excludebox,'string'),ext,'rows');
	if ~isstr(tmp),tmp = ''; end
	set(grepopts.excludebox,'string',tmp);
	if ~strcmp(ext,'.m')
		set(grepopts.monly,'value',0);
	end
	if ~strcmp(ext,'.txt')
		set(grepopts.txtonly,'value',0);
	end
end
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function popallwordsfcn
grepopts = getappdata(gcf,'grepopts');
tmp = get(grepopts.allwordspopup,'value');
if length(tmp) > 1
	beep;
	warndlg('Only one line may be selected.');
else
	tmp2 = get(grepopts.allwordspopup,'string');
	set(grepopts.allwordsbox,'string',tmp2(tmp,:));
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function popmustincludefcn
grepopts = getappdata(gcf,'grepopts');
tmp = get(grepopts.mustincludepopup,'value');
if length(tmp) > 1
	beep;
	warndlg('Only one line may be selected.');
else
	tmp2 = get(grepopts.mustincludepopup,'string');
	set(grepopts.mustincludebox,'string',tmp2(tmp,:));
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function popsomewordsfcn
grepopts = getappdata(gcf,'grepopts');
tmp = get(grepopts.somewordspopup,'value');
if length(tmp) > 1
	beep;
	warndlg('Only one line may be selected.');
else
	tmp2 = get(grepopts.somewordspopup,'string');
	set(grepopts.somewordsbox,'string',tmp2(tmp,:));
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function popmustexcludefcn
grepopts = getappdata(gcf,'grepopts');
tmp = get(grepopts.mustexcludepopup,'value');
if length(tmp) > 1
	beep;
	warndlg('Only one line may be selected.');
else
	tmp2 = get(grepopts.mustexcludepopup,'string');
	set(grepopts.mustexcludebox,'string',tmp2(tmp,:));
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function exclusionsfcn
grepopts = getappdata(gcf,'grepopts');
%ALWAYS IGNORE THESE filenames
tmp = get(grepopts.extensionsbox,'string');
if ~grepopts.overrideexe
	exclusions = strvcat(get(grepopts.excludebox,'string'),'.frk');
	exclusions = strvcat(exclusions,'.exe');
	exclusions = strvcat(exclusions,'.ini');
	exclusions = strvcat(exclusions,'.drv');
	exclusions = strvcat(exclusions,'.sys');
	exclusions = strvcat(exclusions,'.dll');
	exclusions = strvcat(exclusions,'.dat');
end
if get(grepopts.ignorehidden,'value')
	exclusions = strvcat(exclusions,'.db');
	exclusions = strvcat(exclusions,'.inf');
	exclusions = strvcat(exclusions,'.msc');
end
if get(grepopts.smartsearch,'value')  & ~get(grepopts.filenamesonly,'value')
	exclusions = strvcat(exclusions,'.tif');
	exclusions = strvcat(exclusions,'.bmp');
	exclusions = strvcat(exclusions,'.gif');
	exclusions = strvcat(exclusions,'.jpg');
	exclusions = strvcat(exclusions,'.avi');
	exclusions = strvcat(exclusions,'.png');
	exclusions = strvcat(exclusions,'.hdf');
	exclusions = strvcat(exclusions,'.pcx');
	exclusions = strvcat(exclusions,'.xwd');
	exclusions = strvcat(exclusions,'.ico');
	exclusions = strvcat(exclusions,'.cur');                      
end
exclusions = setdiff(exclusions,'.*','rows');
if ~isstr(exclusions),exclusions = ''; end
exclusions = setdiff(exclusions,tmp,'rows');
if ~isstr(exclusions),exclusions = ''; end
grepopts.exclusions = exclusions;
setappdata(gcf,'grepopts',grepopts);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function filenamesonlyfcn
grepopts = getappdata(gcf,'grepopts');
set(grepopts.smartsearch,'value',0);
set(grepopts.commentsonly,'value',0);
set(grepopts.excludecomments,'value',0);
exclusionsfcn;
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tmpline = reconstruct(tmpline,excludecommented,commentchar);
hardreturns = find(double(tmpline) == 10 | double(tmpline) == 13);
tmp=hardreturns(diff(hardreturns)==1);
hardreturns = setdiff(hardreturns,tmp);
if excludecommented
	for ii = length(hardreturns)-1:-1:1
		drawnow;
		tmpline2 = tmpline(hardreturns(ii)+1:hardreturns(ii+1)-1);
		tmp = double(tmpline2);
		tmp = find(tmp>32);
		if length(tmp) > 1
			tmp = tmp(1);
			tmpline2 = tmpline2(tmp:end);
		else
			tmpline2 = [];
		end
		if ~isempty(tmpline2) & strcmp(tmpline2(1),commentchar)
			tmpline(hardreturns(ii):hardreturns(ii+1)-1) = [];
		end
	end
else %commentsonly
	for ii = length(hardreturns)-1:-1:1
		drawnow;
		tmpline2 = tmpline(hardreturns(ii)+1:hardreturns(ii+1)-1);
		tmp = double(tmpline2);
		tmp = find(tmp>32);
		if length(tmp) > 1
			tmp = tmp(1);
			tmpline2 = tmpline2(tmp:end);
		else
			tmpline2 = [];
		end
		if ~isempty(tmpline2) & ~strcmp(tmpline2(1),commentchar)
			tmpline(hardreturns(ii):hardreturns(ii+1)-1) = [];
		end
	end
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function sortfcn(type)
grepopts = getappdata(gcf,'grepopts');
boxinds = [grepopts.resultsbox;...
		grepopts.stringbox;...
		grepopts.datebox;...
		grepopts.sizebox;...
		grepopts.extensionbox];
switch type
	case 'results'
		index = 1;
	case 'string'
		index = 2;
	case 'date'
		index = 3;
	case 'size'
		index = 4;
	case 'extension'
		index = 5;
end
nonindex = setdiff([1:length(boxinds)],index);
vals = get(boxinds(index),'value');
tmp = get(boxinds(index),'string');
tmp = tmp(1:end-1); %Discard trailing space, keeping cell structure.
if index == 3
	a = [];
	for jj = 1:size(tmp,1)
		drawnow;
		a = cat(1,a,datenum(tmp{jj}));
	end
	tmp = a;
elseif index == 4
	a=[];
	for jj = 1:size(tmp,1)
		drawnow;
		a=cat(1,a,str2num(tmp{jj}));
	end
	tmp = a;
end
[y,ii] = sort(tmp);
if index == 3
	a = [];
	for jj = 1:size(tmp,1)
		drawnow;
		a = cat(1,a,datestr(y(jj)));
	end
	if ~isempty(a)
		y = cellstr(a);
	end
elseif index == 4  
	if ~isempty(y)
		y = cellstr(num2str(y));
	end
end
if ~isequal(ii,[1:size(tmp,1)]')
	y{end + 1} = ' '; %Re-append trailing space to avoid obscuring string with horizontal scrollbars.
	set(boxinds(index),'string',y); %Previous sort was reverse
	for jj = 1:length(nonindex)
		drawnow;
		tmp = get(boxinds(nonindex(jj)),'string');
		tmp = tmp(1:end-1); %Discard trailing space, keeping cell structure.
		tmp = tmp(ii);
		tmp{end + 1} = ' ';
		set(boxinds(nonindex(jj)),'string',tmp);
	end
	[c,ia]=intersect(ii,vals);
else %Previous sort was forward
	y = y(end:-1:1);
	y{end + 1} = ' ';
	set(boxinds(index),'string',y);
	for jj = 1:length(nonindex)
		drawnow;
		tmp = get(boxinds(nonindex(jj)),'string');
		tmp = tmp(1:end-1);
		tmp=tmp(ii(end:-1:1));
		tmp{end + 1} = ' ';
		set(boxinds(nonindex(jj)),'string',tmp);
	end
	[c,ia]=intersect(ii(end:-1:1),vals);
end
set(boxinds,'value',ia,'listboxtop',1);
set(grepopts.scrollresults,'value',max(1,grepopts.numitems-30));
changeselectionfcn(type);
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function changeselectionfcn(type)
grepopts = getappdata(gcf,'grepopts');
boxinds = [grepopts.resultsbox;...
		grepopts.stringbox;...
		grepopts.datebox;...
		grepopts.sizebox;...
		grepopts.extensionbox];

if strcmp(get(gcf,'selectiontype'),'open')
	editselectedfcn;
	return;
end

switch type
	case 'results'
		index = 1;
	case 'string'
		index = 2;
	case 'date'
		index = 3;
	case 'size'
		index = 4;
	case 'extension'
		index = 5;
end
nonindex = setdiff([1:length(boxinds)],index);
vals = get(boxinds(index),'value');
vals = setdiff(vals,grepopts.numitems+1);
set(boxinds,'value',vals);
lbt = get(boxinds(index),'listboxtop');
nfiles = sum(cellfun('length',grepopts.filedirs)~=1);
set(boxinds(nonindex),'value',vals,'listboxtop',lbt);
if length(vals) == 1 %& vals ~= grepopts.numitems;
	filenames = get(grepopts.resultsbox,'string');
	set(grepopts.status,'string',sprintf('%i file(s) found matching selected criteria.\nFile %s selected.',nfiles,filenames{vals(1)}));
	set(grepopts.editselectedbutton,'enable','on');
	set(grepopts.hideselectedbutton,'enable','on');
	set(grepopts.keepselectedbutton,'enable','on');
	set(grepopts.deleteselectedbutton,'enable','on');
	set(grepopts.replacestringbutton,'enable','on');
	set(grepopts.savefilebutton,'enable','on');
elseif length(vals)>1
	set(grepopts.status,'string',sprintf('%i file(s) found matching selected criteria.\n%i files selected.',nfiles,length(vals)));
	set(grepopts.editselectedbutton,'enable','on');
	set(grepopts.hideselectedbutton,'enable','on');
	set(grepopts.keepselectedbutton,'enable','on');
	set(grepopts.deleteselectedbutton,'enable','on');
	set(grepopts.replacestringbutton,'enable','on');
	set(grepopts.savefilebutton,'enable','on');
else
	set(grepopts.status,'string',{sprintf('%i file(s) found matching selected criteria.',nfiles),...
			'(Select file(s) to activate EDIT button.)'});
	set(grepopts.editselectedbutton,'enable','off');
	set(grepopts.hideselectedbutton,'enable','off');
	set(grepopts.keepselectedbutton,'enable','off');
	set(grepopts.deleteselectedbutton,'enable','off');
	set(grepopts.replacestringbutton,'enable','off');
	set(grepopts.savefilebutton,'enable','off');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function scrollresultsfcn
grepopts = getappdata(gcf,'grepopts');
boxinds = [grepopts.resultsbox;...
		grepopts.stringbox;...
		grepopts.datebox;...
		grepopts.sizebox;...
		grepopts.extensionbox];
set(grepopts.scrollresults,'value',round(get(grepopts.scrollresults,'value')));
lbt = max(1,grepopts.numitems-29-get(grepopts.scrollresults,'value'));
set(boxinds,'listboxtop',lbt);
% When horizontal scrolling bars are present, sometimes lbt gets shifted on
% the bottoms. The following should fix that.
if length(unique(cell2mat(get(boxinds,'listboxtop'))))~=1
	set(boxinds,'listboxtop',min(cell2mat(get(boxinds,'listboxtop'))));
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function showparameters
grepopts = getappdata(gcf,'grepopts');
set(grepopts.allworsdparam, 'string',get(grepopts.allwordsbox,'string')); 
set(grepopts.exactphraseparam, 'string',get(grepopts.mustincludebox,'string'));
set(grepopts.somewordsparam, 'string',get(grepopts.somewordsbox,'string')); 
set(grepopts.mustexcludeparam, 'string',get(grepopts.mustexcludebox,'string'));
set(grepopts.wholewordsparam, 'value',get(grepopts.wholewords,'value'));
set(grepopts.casesensitiveparam, 'value',get(grepopts.casesensitive,'value'));
set(grepopts.excludecommentedparam, 'value',get(grepopts.excludecomments,'value'));
set(grepopts.commentsonlyparam, 'value',get(grepopts.commentsonly,'value'));
set(grepopts.ignorehiddenparam, 'value',get(grepopts.ignorehidden,'value'));
set(grepopts.smartsearchparam, 'value',get(grepopts.smartsearch,'value'));
set(grepopts.filenamesonlyparam, 'value',get(grepopts.filenamesonly,'value'));
if get(grepopts.ignoredates,'value')
	set(grepopts.dateparam,'string','Ignore');
else
	tmp = get(grepopts.modday,'value');
	if tmp
		set(grepopts.dateparam,'string','Today');
	else
		tmp = get(grepopts.modmonth,'value');
		if tmp
			set(grepopts.dateparam,'string','This month');
		else
			tmp = get(grepopts.modyear,'value');
			if tmp
				set(grepopts.dateparam,'string','This year');
			else
				tmp1 = get(grepopts.datemin,'string');
				tmp2 = get(grepopts.datemax,'string');
				set(grepopts.dateparam,'string',[tmp1, ' -- ', tmp2]);
			end
		end
	end
end
if get(grepopts.ignoresizes,'value')
	set(grepopts.filesizeparam,'string','Ignore');
else
	tmp = get(grepopts.smallfiles,'value');
	if tmp
		set(grepopts.filesizeparam,'string','Small (< 100 KB)');
	else
		tmp = get(grepopts.mediumfiles,'value');
		if tmp
			set(grepopts.filesizeparam,'string','Medium (< 1 MB)');
		else
			tmp = get(grepopts.largefiles,'value');
			if tmp
				set(grepopts.filesizeparam,'string','Large (> 1 MB)');
			else
				tmp = get(grepopts.usekb,'value');
				if tmp,tmp = 'KB';else,tmp = 'MB';end
				tmp1 = get(grepopts.sizemin,'string');
				tmp2 = get(grepopts.sizemax,'string');
				set(grepopts.filesizeparam,'string',[tmp1, ' -- ', tmp2,' ',tmp]);
			end
		end
	end
end

set(findobj('tag','showparameters'),'visible','on');
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function replacestringfcn
verify=questdlg(sprintf('This function will overwrite files irretrievably.\nProceed with caution!\n\nNOTE: Currently tested and operational only for the following file types:\n\n*.m, *.doc, *.txt, *.rtf, *.asc, *.xls.\n\n '),'CAUTION!','CONTINUE','Cancel','CONTINUE');
if strcmp(verify,'Cancel'),return;end 
grepopts = getappdata(gcf,'grepopts');
prompt={'Find what:','Replace with:'};
dlgTitle='Find & Replace';
lineNo=1;
answer=inputdlg(prompt,dlgTitle,lineNo);
if isempty(answer),return;end
targetstring = answer{1};
if isempty(targetstring),return;end
replacementstring = answer{2};
filenames = get(grepopts.resultsbox,'string');
vals = get(grepopts.resultsbox,'value');
testvals = strvcat('.m','.doc','.txt','.rtf','.asc','.xls');
count = 0;
for ii = 1:length(vals)
	drawnow;
	[a,b,c] = fileparts(filenames{vals(ii)});
	if ismember(c,testvals)
		if strcmp(c,'.xls')
			Excel = actxserver('Excel.Application'); 
			Excel.Visible = 0; 
			w = Excel.Workbooks; 
			tmpexcel = invoke(w, 'open', fullfile(a,[b,c])); 
			archive = Excel.Activesheet; 
			archive.Unprotect; 
			% Get the value of the next available line in the spreadsheet:
			readinfo = get(archive,'UsedRange');
			tmp = readinfo.value;
			for ii=1:prod(size(tmp))
				if ischar(tmp{ii})
					tmp{ii}=strrep(tmp{ii},targetstring,replacementstring);
				end
			end
			readinfo.value=tmp;
			release(readinfo); 
			%invoke(tmpexcel,'save');
			release(tmpexcel);
			invoke(Excel, 'quit'); 
			release(archive); 
			release(w); 
			delete(Excel); 
			drawnow;
			count = count+1;
			continue
		elseif strcmp(c,'.doc')
			% 			msword = actxserver('Word.Application'); 
			% 			msword.Visible = 0; 
			% 			Documents = msword.Documents;
			% 			invoke(Documents,'Open', fullfile(a,[b,c]));
			% 			% Get the value of the next available line in the spreadsheet:
			% 			readinfo = get(archive,'UsedRange');
			% 			tmp = readinfo.value;
			% 			for ii=1:prod(size(tmp))
			% 				if ischar(tmp{ii})
			% 					tmp{ii}=strrep(tmp{ii},targetstring,replacementstring);
			% 				end
			% 			end
			% 			readinfo.value=tmp;
			% 			release(readinfo); 
			% 			%invoke(tmpexcel,'save');
			% 			release(tmpexcel);
			% 			invoke(Excel, 'quit'); 
			% 			release(archive); 
			% 			release(w); 
			% 			delete(Excel); 
			% 			drawnow;
			count = count+1;
			continue
		end
		count = count+1;
		set(grepopts.status,'string',sprintf('Replacing string ''%s'' with ''%s''in file %s...',targetstring,replacementstring,filenames{vals(ii)}));
		fid = fopen(filenames{vals(ii)},'r+');
		s1 = fread(fid);
		s1 = char(s1');
		s1=strrep(s1,targetstring,replacementstring);
		fclose(fid);
		delete(filenames{vals(ii)});
		fid = fopen(filenames{vals(ii)},'w');
		fprintf(fid,'%s',s1);
		fclose(fid);
		pause(1);
	else
		set(grepopts.status,'string',sprintf('Skipping file %s....',filenames{vals(ii)}));
		pause(1);
	end
end %for ii = 1:length(vals)
if count ~= 0
	set(grepopts.status,'string',sprintf('Done! String replacement completed in %d files.',count));
else
	set(grepopts.status,'string','No files with valid extensions selected for string replacement.');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function savefilefcn
global overwrite
grepopts = getappdata(gcf,'grepopts');
filenames = get(grepopts.resultsbox,'string');
filedates = get(grepopts.datebox,'string');
vals = get(grepopts.resultsbox,'value');
count = 0;
skipped = 0;
overwrite = 1;
writedir = uigetdir('Select disk/directory for saving');
for ii = 1:length(vals)
	drawnow;
	[a,b,c] = fileparts(filenames{vals(ii)});
	if ~isempty(dir(filenames{vals(ii)})) %is the file found? (This is necessary so program doesn't bomb on hidden files)
		try
			tmp=dir([writedir,filesep,b,c]);
			if ~isempty(tmp) %A file exists in the same location with the same name
				existingdate = datenum(tmp.date);
				newdate = datenum(filedates{vals(ii)});
				if existingdate >= newdate
					if overwrite ~= 3 & overwrite ~= 4
						if existingdate == newdate, msgstr2 = 'the same'; else, msgstr2 = 'a more current'; end
						[objpos,objdim] = distribute(4,0.1,0.9,0.02);
						msgstr = sprintf('WARNING: A destination file with %s datestamp already exists!.\n\nOverwrite file\n%s created on %s with file\n%s, created on %s?\n',msgstr2,[writedir,filesep,b,c],tmp.date,filenames{vals(ii)},datestr(newdate));
						tmpquest = figure('numbertitle','off','menubar','none','name','Verify Overwrite','units','normalized','position',[0.2 0.6 0.6 0.2],'color',[0.82549 0.813725 0.747059]);
						uicontrol('style','text','units','normalized','position',[0.05 0.55 0.9 0.35],'string',msgstr,'backgroundcolor',[0.82549 0.813725 0.747059],'horizontalalignment','left','fontsize',12)
						uicontrol(tmpquest,'style','pushbutton','units','normalized','position',[objpos(1) 0.1 objdim 0.2],'string','YES','callback',...
							['global overwrite; overwrite = 1; delete(gcf);']);
						uicontrol(tmpquest,'style','pushbutton','units','normalized','position',[objpos(2) 0.1 objdim 0.2],'string','SKIP','callback',...
							['global overwrite; overwrite = 2; delete(gcf);']);
						uicontrol(tmpquest,'style','pushbutton','units','normalized','position',[objpos(3) 0.1 objdim 0.2],'string','YES TO ALL','callback',...
							['global overwrite; overwrite = 3; delete(gcf);']);
						uicontrol(tmpquest,'style','pushbutton','units','normalized','position',[objpos(4) 0.1 objdim 0.2],'string','SAVE NEWER FILES ONLY','callback',...
							['global overwrite; overwrite = 4; delete(gcf);']);
						uiwait(tmpquest);
					end
				end
				if overwrite == 2 | (overwrite == 4 & existingdate >= newdate)
					skipped = skipped + 1;
					fprintf('Skipped file "%s" (%d of %d)\n',filenames{vals(ii)},ii,length(vals));
					set(grepopts.status,'string',sprintf('Skipped file "%s" (%d of %d)\n',filenames{vals(ii)},ii,length(vals)));
					drawnow;
					continue
				end
			end
			[success,message] = copyfile(filenames{vals(ii)},[writedir,filesep,b,c]);
			if ~success %Force error
				pause(2);
				set(grepopts.status,'string',sprintf('PROBLEM ENCOUNTERED copying file "%s" to "%s". (File will be skipped.)\n(%d of %d)',filenames{vals(ii)},[writedir,filesep,b,c],ii,length(vals)));
				error;
			end
			set(grepopts.status,'string',sprintf('Copying file "%s" to "%s"\n(%d of %d)',filenames{vals(ii)},[writedir,filesep,b,c],ii,length(vals)));
			count = count + 1;
		catch
			skipped = skipped + 1;
			fprintf('Skipped file "%s" (%d of %d)\n',filenames{vals(ii)},ii,length(vals));
			set(grepopts.status,'string',sprintf('Skipped file "%s" (%d of %d)\n',filenames{vals(ii)},ii,length(vals)));
			pause(2);
		end
	else
		skipped = skipped + 1;
		beep;
		fprintf('Skipped file "%s" (%d of %d)\n',filenames{vals(ii)},ii,length(vals));
		set(grepopts.status,'string',sprintf('Skipped file "%s" (%d of %d)\n%s',filenames{vals(ii)},ii,length(vals),'FILE NOT FOUND!'));
		pause(2);
	end % if ~isempty(dir(filenames{vals(ii)}))
end %for ii = 1:length(vals)
if count ~= 0
	set(grepopts.status,'string',sprintf('Done! %d files saved to disk (%d files skipped)!',count,skipped));
else
	set(grepopts.status,'string','No files saved.');
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function openlist = openwith(ext)
% function openlist = openwith(ext)
% Thanks to Richard Alcock and Bob Gilmore for their assistance in response to my queries about detecting file associations

if ~nargin | ~strcmp(class(ext),'char')
	error('Function takes one argument, which must be a string indicating the extension. E.g., ''.avi''');
end
if ~strcmp(ext(1),'.')
	ext = ['.' ext];
end

alph = 'abcdefghijklmnopqrstuvwxyz';
openlist = [];
for ii = 1:length(alph)
	drawnow;
	try
		tmp = winqueryreg('HKEY_CURRENT_USER',sprintf('Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\%s\\OpenWithList',ext),alph(ii));
		openlist = strvcat(openlist,tmp);
	catch
		if isempty(openlist)
			fprintf('No file associated with extension %s.\n',ext);
			return;
		end
		openlist = setdiff(openlist,'iexplore.exe','rows');
		return
	end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function p = genpath(d)
%GENPATH Generate recursive toolbox path.
%   P = GENPATH returns a new path string by adding
%   all the directories below MATLABROOT/toolbox.
%
%   P = GENPATH(D) returns a path string formed by 
%   recursively adding all the directories below D.

%   Copyright 1984-2001 The MathWorks, Inc. 
%   $Revision: 104 $ $Date: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $

%NOTE: This function is pasted here as a subfunction so I can overload the functionality
% of genpath. The MATLAB version skips 'private' and 'class' directories. I want them to 
% be included. Also, I want to include a 'drawnow' command in the for-loop
% for greater control of interruptibility.
% Generate path based on given root directory. Comments (except for credits) have been
% stripped; see original function for comments.
files = dir(d);
if isempty(files)
	return
end
p = '';
isdir = logical(cat(1,files.isdir));
if ~all(isdir)
	p = [p d pathsep];
end
dirs = files(isdir);
for i=1:length(dirs)
	dirname = dirs(i).name;
	if ~strcmp( dirname,'.')         &...
			~strcmp( dirname,'..')
		p = [p genpath(fullfile(d,dirname))];
	end
	drawnow;
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function mode = mode(a)
% Returns the mode (most frequent element(s)) of matrix a
% Written by Brett Shoelson, Ph.D.
% 11/29/01
[n,x] = hist(a(:),sort(a(:)));
mode = x(find(n==max(n)));
return;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [objpos,objdim] = distribute(nobjects,startpos,endpos,gap);
objdim = ((endpos-startpos)-(nobjects-1)*gap)/nobjects;
objpos = [startpos:objdim+gap:endpos];
if any(objpos < 0) | objdim < 0
	warndlg('The parameters you entered result in a negative starting point or dimension. You may want to rethink that.');
end
return
