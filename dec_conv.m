%%
% Interactive script to convert decimal numbers to twos complement
% format with a binary point at the speicifed location
%
% Original by Elliot Schei
% Modified by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

disp(' ')
x = input('please input a base 10 number: ');
res = input('please input the resolution of binary point (16 or 32): ');
int_x = floor(x); % get the xxx.
dec_x = x - int_x; % get the .xxx
C = dec_x; % temporarily store .xxx 
dec_z = zeros(1,res); % create a variable for .xxx conversion
i = 1; % set a value for iteration

while i < res,
    A = C*2;
    B = floor(A);
    dec_z(1,i) = B;
    if B > 0 
       C = A - B;
    else
       C = A;
    end
    i = i + 1;
end

int_z = dec2bin(int_x);
str_decz = setstr(dec_z+'0');
 
clc;
fprintf('\n the binary term is %s.%s \n',int_z,str_decz);
disp (' ');
