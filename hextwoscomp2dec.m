function out_val = hextwoscomp2dec(in_val)

%%
% This is a script to convert in twos comp numbers in Hex
%  format to decimal format, for number up to 32 bits.
%
% Usage: hextwoscomp2dec('HEXNUM')
%
% Orginal by Chris Arndt
%
% $LastChangedDate: 2008-09-11 12:40:43 -0600 (Thu, 11 Sep 2008) $
% $Rev: 572 $
%

%converting the first character of the hex string to a decimal number
first_char = hex2dec(in_val(1));
%converting entire input to decimal
dec_val = hex2dec(in_val);

if first_char > 7 %if the number is negative
    num_bits = ceil(log2(dec_val));
    if (first_char == 8) && (mod(num_bits,4) ~= 0)
        %if the first_char is an '8' and numb_bits isn't divisible by 4
        %adding and additional bit if the number is a power of 2
        %this is needed for th '0x8', '0x80', 0x800', etc cases
        num_bits = num_bits + 1;
    end
    %complement and increment to get the abs value, then negate
    out_val = -(bitcmp(dec_val,num_bits) + 1);
else %if the number is positive
    %simply convert to decimal
    out_val = dec_val;
end
