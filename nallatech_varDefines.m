%%
% Nallatech defines needed for XtremeDSP Development Kit Scripts
%
% Original by Andy Khan
% Modified by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

def.dr.DISABLE		 = hex2dec('00000000');
def.dr.ENABLE		 = hex2dec('00000001');
def.dr.TOGGLE		 = hex2dec('00000002');

def.dr.CONTROLABLE	 = hex2dec('00000003');
def.dr.TOGGLEONLY	 = hex2dec('00000004');

def.dr.INTERFACE	 = hex2dec('00000001');
def.dr.SYSTEM		 = hex2dec('00000002');
def.dr.ONBOARDFPGA	 = hex2dec('00000004');

def.dl.dlPCI		 = hex2dec('00000001');
def.dl.dlUSB		 = hex2dec('00000002');
def.dl.dlTCPIP		 = hex2dec('00000004');
def.dl.dlCITRIX          = hex2dec('00000005');
def.dl.dlETHERNET	 = hex2dec('00000006');
def.dl.dlVME		 = hex2dec('00000007');

def.dl.NUMCARDS          = hex2dec('10000000'); %Returns the number of cards located. No card number required.
def.dl.MBTYPE		 = hex2dec('10000001'); %Returns the Motherboard Type for the specified card.
def.dl.INTERFACE	 = hex2dec('10000002'); %Returns the LocateType for the card eg dlPCI,dlUSB.
def.dl.SERIALNUMBER	 = hex2dec('10000003'); %Returns the SerialNumber of the card.
def.dl.DRIVERVERSION     = hex2dec('10000004'); %Returns the def.driver version number for the card.
def.dl.DESCRIPTION	 = hex2dec('10000005'); %Returns a short discription of the card def.driver. Only valid for StatusPtr function.


def.mbt.NONE               = hex2dec('00000000');
def.mbt.THEBALLYNUEY       = hex2dec('00000001');
def.mbt.THEBALLYNUEY2      = hex2dec('00000002');
def.mbt.THEBALLYINX        = hex2dec('00000003');
def.mbt.THESTRATHNUEY      = hex2dec('00000004');
def.mbt.THEBENERAPROTOTYPE = hex2dec('00000005');
def.mbt.THEBENADIC         = hex2dec('00000006');
def.mbt.THEBALLYNUEY3      = hex2dec('00000007');
def.mbt.THEBENERA          = hex2dec('00000008');
def.mbt.THEBENNUEY         = hex2dec('00000009');
def.mbt.THEBENONE          = hex2dec('0000000A');
def.mbt.THEBENNUEYPC104    = hex2dec('0000000D');
def.mbt.THEBENNUEY4E       = hex2dec('0000000E');
def.mbt.THEBENNUEYVME      = hex2dec('0000000F');
def.mbt.ALL                = hex2dec('ffffffff');

def.inf.MODULETEMP         = hex2dec('14000009');
def.inf.FPGATEMP           = hex2dec('1400000A');
