% Matlab Script for calculating Normalized Radix-2 FFTs
%
% Original by Brian Hoard 8/26/2003
% Modified by Chris Anrdt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

function fft_demo(input)

% Step #1: Make sure array is a power of 2

N = length(input);
N2 = length(input);

x1 = log2(N); x2 = round(x1);
if x1 ~=x2
    error('ERROR: Vector length must be a power of 2');
end

        
% Step 2: Roots of Unity - Store the values of Wn:

% Note: Calculated later in the loops

% Step 3: Perform bit reversal of input

count=(0:N-1); 
count_binary = dec2bin(count); 
reverse_count = fliplr (count_binary); % Flip the matrix dec2bin creates
count_bin=bin2dec(reverse_count)+1; 
disp(' ');disp('Problem 1: Part a');
disp('Bit-reveresed ordering of the indices =');
disp(' '), disp(count_bin-1), disp(' ');

x = input(count_bin);  % Use new count to re-order input

% Step 4: Do the FFT stages of length 2 up to 2^N-1
       
for k=1:1:log2(N)
    
% Show the Butterfly Outputs:
% Note, stage 0 = the rearranged inputs
disp(['Butterfly Stage = ', num2str(k-1)]);
disp('Output = ');
disp(x);disp(' ');

N=N/2;
 
% Solve the Roots of Unity
    W = exp(-i*2*pi*N/N2);
        for f=0:1:(N2/(2*N))-1   
            Wn(f+1) = W^f';
        end
        
    for z = 0:1:N-1
        
        for p=1:1:(2^k)/2
       
            x_temp((2^k)*z+p) = x((2^k)*z+p)+Wn(p)*x((2^k)*z+p+(2^k)/2);
            x_temp((2^k)*z+p+(2^k)/2) = x((2^k)*z+p)-Wn(p)*x((2^k)*z+p+(2^k)/2);
            
            x((2^k)*z+p)=x_temp((2^k)*z+p);
            x((2^k)*z+p+(2^k)/2)=x_temp((2^k)*z+p+(2^k)/2);  
           
        end
    end
end
end

% Display last Butterfly operation:
    x=conj(x);
disp(['Butterfly Stage = ', num2str(k)]);
disp('Output = ');
disp(x)




% Multiply x by 1/length(input) to normalize
format short
X_final = (1/length(input))*x;
disp(' '); disp('The result of the DFT = ');
disp(' '); disp(X_final); disp(' ');




