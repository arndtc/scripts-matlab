function plot_fft(X,N);
% PLOT_FFT plots a fft function.
%
% PLOT_FFT only implements a few of the fft function's features.
%   PLOT_FFT(X,N) is the N-point FFT, padded with zeros if X has less
%   than N points and truncated if it has more.
%
% Type 'help FFT' for more informationon the fft function.
%
% Orignial by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

%calculate fft and axis
fft_data=fft(X,N);
fft_magnitude=sqrt((real(fft_data).^2 + imag(fft_data).^2));
x_axis=(1:N)';

%plot fft
plot(x_axis,fft_magnitude);
axis tight;
