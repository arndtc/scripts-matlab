function out = dec2twoscomp(x, n, b)
%Decimal to Binary Conversion 
%   dec2twoscomp(x, n, b) turns any Decimal number (x) in to a 2's complement
%   Binary number of the specified number number of bits (n), with the
%   binary point being (b) positions to the left of the LSB.
%
%   The Syntax is:
%              C = dec2twoscomp(10.5, 8, 3)
%    result -> C = 01010100   
%
%   or
%            C = dec2twoscomp(x)          where x=[ 1 -2 3 5]
%
%   Fractional values that can not be represented by the number
%   scheme chosen are rounded to there nearest value.
%
%   If (n) is not specified, it is calculated for the largest value in 
%   (x) so all numbers can be represented. (x) can be an array of numbers
%   as well. 
%
%   If (b) is not specified the default value of 0 is used.
%   Truncation is not supported
%
% Original by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

%% Initialise Variables
flag =0;
j = 0;
i = 0;
  % If N is not specified calculate it.
  % Calculate "n" for the largest value in "x" so all numbers can be
  %represented
  if nargin < 2, 
      m=max(abs(x));
            for n = 1:1:128,
                result = m/((2^n)-1);
                if result <= 1
                    break 
               end
           end
n=n+1; %+1 to cover the 2's complement number
b = 0;  % Default is zero
x = round(x);  % remove any fractional values as "b=0"
elseif nargin < 3,
          b = 0;  % Default is zero
          x = round(x);  % remove any fractional values as "b=0"
      else
%SCALE VALUES i.e. MOVE DECIMAL POINT TO THE END OF THE NUMBER.
      x = x*2^b;
      x = round(x);
  end
  %-----------------------------------
    for row = 1:length(x);
    % IF NEGATIVE -> TURN TO POSITIVE
   if (x(row) < 0)
        flag = 1;  % NEGATIVE NUMBER
        x(row) = abs(x(row));
    else
        flag = 0;
    end
        
    j(row,1) = 0; %sign extend

   for i = n:-1:2,
      j(row,i) = mod(x(row), 2);
      x(row) = x(row) / 2;
      x(row) = floor(x(row));
    end
% IF NEGATIVE FIND THE 2'S COMPLEMENT
   if (flag == 1) 
       %inverse the bits
       for k = n:-1:1,
            if (j(row,k)==1)
               j(row,k)=0;
            else
               j(row,k)=1;
           end
       end
%add one
       for k = n:-1:1,
             if (j(row,k)==0)
                  j(row,k)=1;
                  break
              else
                j(row,k)=0;
            end
       end
   end            
end
binpt = (n-b)+1;  %assign binary point graphical display position.
test(1,1) = '1';  % initialise display cell of characters
for row = 1:length(x),
%% OTO mod----------------------------------------------------------
   %for h = 1:n+1, %n+1 covers the extra position for the binary pt.
    for h = 1:n, %no position for binary point
       if (b==0), %Special case for no binary point
           test1(row,h) = char(num2str(j(row,h)));
                if (h==n), %as no binary pt can not exceed matrix
               break 
           end
%% OTO modification-------------------------------------------
elseif (h == binpt), %insert -*IMAGINARY*- binary point 
           test1(row,h) = char(num2str(j(row,h)));
       elseif (h > binpt),
         
             test1(row,h) = char(num2str(j(row,h)));
         else
             test1(row,h) = char(num2str(j(row,h)));
         end
   end
end
out = test1;

