%% -- Added by System Generator automatically during installation -- %%
%% --                                                             -- %%
%% --                Do not remove this banner                    -- %%
%% --                                                             -- %%
xilinx_dsp = getenv('XILINX_DSP');
if isempty(xilinx_dsp)
   error('XILINX_DSP environment variable is not defined!');
end

sysgenbin  = [ xilinx_dsp filesep 'sysgen' filesep 'bin' ];

if exist(sysgenbin, 'dir')
   addpath(sysgenbin);
   try
      xlAddSysgen(xilinx_dsp);
      disp('Installed System Generator dynamically.');
   catch
      disp('Error occured while attempting to install System Generator into MATLAB path.');
      lasterr;
   end
   clear xilinx_dsp;
   clear sysgenbin;
   lasterr('');
else
   disp('Could not find bin directory for System Generator');
   disp('to be added into MATLAB paths.  Please check your XILINX_DSP');
   disp('environment variable.');
   clear sysgenbin;
end
%---------------------------------------------------------------
