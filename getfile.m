function getfile(filename)
% GETFILE             Copy a file to the current directory
%
% GETFILE filename places a copy of filename in the current directory.
% filename must be on the MATLAB path.  This is easier than using
% COPYFILE because you don't need to specify the path to the source file.
%
% Example:
%   getfile contour  %Place a copy of contour.m in the current directory
%
% SEE ALSO: GOTO (MATLAB Central), WHICH, COPYFILE

% Original by Scott Hirsch shirsch@mathworks.com
% Modified by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%

fn = which(filename);
if isempty(fn)
    error([filename ' not found.']);
end;

[s,msg] = copyfile(which(filename));
if s==0
    error(msg)
end;

