function xlFindRPMs(subsystemName)
%% Find all Xilinx blocks in a subsystem or design that use RPMs
% 
%  Usage:  xlFindRPMs(gcs)
%  Usage:  xlFindRPMs(<model, subsystem name, or gcs>)
%
% For example, to find all the RPMs in the mod_sys design use the following command:
%  xlFindRPMs('mod_sys')
%
% original by Nabeel Sharazi
% Modified by Chris Arndt
%
% $LastChangedDate: 2006-07-18 09:53:47 -0600 (Tue, 18 Jul 2006) $
% $Rev: 104 $
%   

%% Find all the blocks using RPMs 
blks = find_system(subsystemName,'LookUnderMasks', 'all', 'use_rpm', 'on');  
% To push into libraries, add the following option:  'FollowLinks', 'on',

numRPMs = length(blks);
if (numRPMs > 0)
    disp('The following  blocks are using RPMs:');
    for i = 1:numRPMs
        if (iscell(blks))
            blk = blks{i};
        else
            blk = blks(i);
        end;
        str = sprintf('%s', blk);
        disp(str);
    end;
    str = sprintf('Total number of RPMs: %d\n', numRPMs);
    disp(str);
end;


%% Find all the block Not using RPMs 
blks = find_system(subsystemName,'LookUnderMasks', 'all', 'use_rpm', 'off');
numNonRPMs = length(blks);
if (numNonRPMs > 0)
    disp('The following  blocks have the option to use RPMs but have the option turned OFF:');
    for i = 1:numNonRPMs
        if (iscell(blks))
            blk = blks{i};
        else
            blk = blks(i);
        end;
        str = sprintf('%s', blk);
        disp(str);
    end;
end;
    
return;
